﻿using UnityEditor;
using VuxtaStudio.Common.Editor;
using VuxtaStudio.TDS.WeaponSystem;

namespace VuxtaStudio.TDS.WeaponSystem.Editor
{
    static class WeaponProfileAssetCreate
    {
        [MenuItem("Assets/Create/TDS/WeaponSystem/WeaponProfile")]
        public static void CreateWeaponProfileAsset()
        {
            ScriptableObjectUtility.CreateAsset<WeaponProfile>();
        }
    }
}
