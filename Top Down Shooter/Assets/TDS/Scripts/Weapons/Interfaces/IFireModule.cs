﻿// <copyright file="IFireModule.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>01/08/2019 16:57:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, Janunary 08, 2019

using UnityEngine;

namespace VuxtaStudio.TDS.WeaponSystem
{
    /// <summary>
    /// Interface of fire module
    /// </summary>
    public interface IFireModule
    {
        void FireAt(Vector3 targetPos);
        void Fire(Vector2 direction);
        void Reload();
        void Switch();
    }
}