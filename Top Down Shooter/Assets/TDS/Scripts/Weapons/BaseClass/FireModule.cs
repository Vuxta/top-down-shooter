﻿// <copyright file="FireModuleBase.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>01/08/2019 16:57:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, Janunary 08, 2019

using UnityEngine;
using System.Collections;
using VuxtaStudio.Common.FiniteStateMachine;
using System.Collections.Generic;

namespace VuxtaStudio.TDS.WeaponSystem
{
    /// <summary>
    /// Base class weapon firing modules
    /// </summary>
    public class FireModule : MonoBehaviour, IFireModule
    {
        #region Private Serialized Fields

        [SerializeField]
        private WeaponProfile[] m_WeaponProfiles;

        #endregion

        #region Private Fields

        private StateMachine m_WeaponMachine;
        private List<WeaponModel> m_WeaponModels = new List<WeaponModel>();
        private int m_CurrentWeaponIndex = 0;

        #endregion

        #region Unity MonoBehaviour Methods

        /// <summary>
        /// Initialize internal clip/storage ammos and other variables
        /// </summary>
        private void Awake()
        {
            m_WeaponMachine = new StateMachine();

            for (int i = 0; i < m_WeaponProfiles.Length; i++)
            {
                WeaponModel model = new WeaponModel(m_WeaponProfiles[i]);
                m_WeaponModels.Add(model);
            }

            m_WeaponMachine.ChangeState(m_WeaponModels[m_CurrentWeaponIndex]);
        }

        private void FixedUpdate()
        {
            m_WeaponMachine.OnMachineUpdate();
        }

        #endregion

        #region Public Methods

        public void FireAt(Vector3 targetPos)
        {
            //TODO: ignore self
            Vector3 testPosition = transform.position;
            Vector3 fireDirection = targetPos - testPosition;
            m_WeaponModels[m_CurrentWeaponIndex].FireAt(testPosition, fireDirection);
        }

        /// <summary>
        /// Fire to certian direction
        /// </summary>
        /// <param name="direction">Direction to fire to</param>
        public void Fire(Vector2 direction)
        {
            Vector3 fireDirection = Vector3.Project(new Vector3(direction.x, 0f, direction.y), transform.forward);
            FireAt(transform.position + fireDirection);
        }

        /// <summary>
        /// Reload clip ammos
        /// </summary>
        public void Reload()
        {
            m_WeaponModels[m_CurrentWeaponIndex].Reload();
        }

        public void Switch()
        {
            ++m_CurrentWeaponIndex;

            if (m_CurrentWeaponIndex >= m_WeaponModels.Count)
                m_CurrentWeaponIndex = 0;

            m_WeaponMachine.ChangeState(m_WeaponModels[m_CurrentWeaponIndex]);
        }

        #endregion
    }
}
