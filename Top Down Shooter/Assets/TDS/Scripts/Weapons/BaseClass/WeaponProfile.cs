﻿using UnityEngine;
using System.Collections;

namespace VuxtaStudio.TDS.WeaponSystem
{
    public class WeaponProfile : ScriptableObject
    {
        public GameObject BulletPrefab;
        public int ClipAmmos = 12;
        public int StorageAmmo = 60;
        public bool InfiniteClipAmmo = false;
        public bool InfiniteStorageAmmo = false;
        public float ReloadTime = 5f;
        public float Distance = 1.2f;
        public float TimeBetweenFire = 0.3f;
    }
}
