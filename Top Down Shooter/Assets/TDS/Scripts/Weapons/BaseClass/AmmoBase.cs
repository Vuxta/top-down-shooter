﻿using System.Collections.Generic;
using UnityEngine;
using VuxtaStudio.TDS.PhysicsSystem;

namespace VuxtaStudio.TDS.WeaponSystem
{
    /// <summary>
    /// Base class for ammo
    /// </summary>
    [RequireComponent(typeof(DamagerComponent))]
    public abstract class AmmoBase : MonoBehaviour
    {
        #region Private Serialized Fields

        [SerializeField]
        protected float m_Speed = 14f;

        [SerializeField]
        protected float m_LifeTime = 5f;

        #endregion

        private DamagerComponent m_DamagerComponent;

        private void Start()
        {
            if (m_DamagerComponent == null)
                m_DamagerComponent = GetComponent<DamagerComponent>();

            m_DamagerComponent.OnDamagerHit.AddListener(OnDamagerHit);

            SetInitVelocity();

            Destroy(gameObject, m_LifeTime);
        }

        protected abstract void SetInitVelocity();

        /// <summary>
        /// Callback function when damager hits
        /// </summary>
        /// <param name="damagableList">List of Damagable components</param>
        /// <param name="collision">collision information when hit</param>
        protected virtual void OnDamagerHit(List<DamagableComponent> damagableList, Collider collision)
        {
            Destroy(gameObject);
        }
    }
}
