﻿using UnityEngine;
using System.Collections;
using VuxtaStudio.Common.FiniteStateMachine;

namespace VuxtaStudio.TDS.WeaponSystem
{
    public class WeaponModel : IState
    {
        private WeaponProfile m_Profile;

        private int m_CurrentClipAmmo;
        private int m_CurrentStorageAmmo;

        private bool m_Reloading = false;

        private IEnumerator m_ReloadCoroutine;
        private YieldInstruction m_ReloadYieldInstruction;

        private float m_NextFireTime = 0f;
        private float m_ReloadCounter = 0f;

        public WeaponModel(WeaponProfile profile)
        {
            m_Profile = profile;
            m_CurrentClipAmmo = m_Profile.ClipAmmos;
            m_CurrentStorageAmmo = m_Profile.StorageAmmo;
        }

        private void FinishReloading()
        {
            if (m_Profile.InfiniteStorageAmmo)
            {
                m_CurrentClipAmmo = m_Profile.ClipAmmos;
            }
            else
            {
                int ammoToFill = m_Profile.ClipAmmos - m_CurrentClipAmmo;

                ammoToFill = ammoToFill <= m_CurrentStorageAmmo ? ammoToFill : m_CurrentStorageAmmo;
                m_CurrentStorageAmmo -= ammoToFill;
                m_CurrentClipAmmo += ammoToFill;
            }

            m_ReloadCounter = 0;
            m_Reloading = false;
        }

        public bool FireAt(Vector3 from, Vector3 dir)
        {
            if (!m_Profile.InfiniteClipAmmo)
            {
                if (m_CurrentClipAmmo <= 0 || m_Reloading)
                    return false;

                if (Time.time < m_NextFireTime)
                    return false;

                --m_CurrentClipAmmo;
            }

            m_NextFireTime = Time.time + m_Profile.TimeBetweenFire;
            GameObject bulletObj = Object.Instantiate(m_Profile.BulletPrefab);

            bulletObj.transform.position = from + dir.normalized * m_Profile.Distance;
            bulletObj.transform.forward = dir;

            if (m_CurrentClipAmmo <= 0)
            {
                Reload();
            }

            return true;
        }

        public void Reload()
        {
            if (!m_Profile.InfiniteClipAmmo)
            {
                m_ReloadCounter = 0;
                m_Reloading = true;
                m_NextFireTime = Time.time;
            }
        }

        public void OnStateEnter()
        {
            m_ReloadCounter = 0f;
        }

        public void OnStateUpdate()
        {
            if (m_CurrentStorageAmmo <= 0 && !m_Profile.InfiniteStorageAmmo)
            {
                return;
            }

            if (!m_Reloading)
                return;

            m_ReloadCounter += Time.fixedDeltaTime;

            if (m_ReloadCounter > m_Profile.ReloadTime)
            {
                FinishReloading();
            }
        }

        public void OnStateExit()
        {
        }
    }
}
