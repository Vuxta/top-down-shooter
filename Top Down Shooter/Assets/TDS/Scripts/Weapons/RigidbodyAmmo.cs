﻿using UnityEngine;

namespace VuxtaStudio.TDS.WeaponSystem
{
    /// <summary>
    /// Ammo using rigidbody
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class RigidbodyAmmo : AmmoBase
    {
        protected override void SetInitVelocity()
        {
            Rigidbody rigidbody = GetComponent<Rigidbody>();

            rigidbody.velocity = rigidbody.transform.forward * m_Speed;
        }
    }
}
