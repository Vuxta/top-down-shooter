﻿using UnityEngine;
using VuxtaStudio.Common.InputSystems;

namespace VuxtaStudio.TDS.PlayerControl
{
    public class DesktopInputProfile : InputSystemProfile
    {
        public DesktopInputProfile(Transform centerTransform = null)
        {
            SimpleButtonAxisHandler directionAxisHandler = new SimpleButtonAxisHandler(new KeyboardButtonHandler(KeyCode.D),
                                                                                    new KeyboardButtonHandler(KeyCode.A),
                                                                                    new KeyboardButtonHandler(KeyCode.W),
                                                                                    new KeyboardButtonHandler(KeyCode.S));

            IAxisHandler axisHandler;

            if (centerTransform == null)
            {
                axisHandler = new ScreenAxisHandler();
            }
            else
            {
                axisHandler = new TransformAxisHandler(centerTransform);
            }

            RegisterAxisHandler(PlayerInputID.DirectionAxis, directionAxisHandler);
            RegisterAxisHandler(PlayerInputID.AimAxis, axisHandler);
            RegisterButtonHandler(PlayerInputID.Attack, new KeyboardButtonHandler(KeyCode.Mouse0));
            RegisterButtonHandler(PlayerInputID.Reload, new KeyboardButtonHandler(KeyCode.R));
            RegisterButtonHandler(PlayerInputID.SwitchWeapon, new KeyboardButtonHandler(KeyCode.Q));
        }
    }
}
