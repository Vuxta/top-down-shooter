﻿using UnityEngine;

namespace VuxtaStudio.TDS.PlayerControl
{
    /// <summary>
    /// Base class for player controllable movement component
    /// </summary>
    public abstract class PlayerMovementComponent : PlayerControllableComponent
    {
        [Header("Movement")]

        [SerializeField]
        protected float m_MaximumSpeedAtGround = 5f;

        [SerializeField]
        protected float m_MovementMultiplier = 1f;

        [SerializeField]
        protected float m_MaximumTurnSpeed = 360f;

        private void FixedUpdate()
        {
            if (m_InputSystem == null)
                return;

            if (!m_ControlEnable)
                return;

            UpdateMovement();
        }

        /// <summary>
        /// Movement/rotation overidding function updated in fixed update
        /// </summary>
        protected abstract void UpdateMovement();
        public abstract Transform GetTransform();
    }
}
