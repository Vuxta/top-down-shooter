﻿using UnityEngine;

namespace VuxtaStudio.TDS.PlayerControl
{
    /// <summary>
    /// Player movement component using unity rigidbody
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerRigidbodyMovementComponent : PlayerMovementComponent
    {
        [SerializeField]
        private Rigidbody m_playerRigidbody;
        private Vector3 m_desiredMovement;

        private void Start()
        {
            if (m_playerRigidbody == null)
                m_playerRigidbody = GetComponent<Rigidbody>();
        }

        public override Transform GetTransform()
        {
            return m_playerRigidbody.transform;
        }

        protected override void UpdateMovement()
        {
            MovePlayer(m_InputSystem.GetXYAxis(PlayerInputID.DirectionAxis));
            TurnPlayer(m_InputSystem.GetXYAxis(PlayerInputID.AimAxis));
        }

        /// <summary>
        /// Move player by the given direction axis
        /// </summary>
        /// <param name="desiredDirection"></param>
        private void MovePlayer(Vector2 desiredDirection)
        {
            if (desiredDirection.sqrMagnitude <= 0.00001f)
            {
                m_playerRigidbody.velocity = Vector3.zero;
                return;
            }

            Vector3 movement = new Vector3(desiredDirection.x, 0f, desiredDirection.y);
            movement *= m_MovementMultiplier;
            movement += m_playerRigidbody.velocity;
            movement.y = 0f;

            if (movement.sqrMagnitude < m_MaximumSpeedAtGround * m_MaximumSpeedAtGround)
            {
                movement = movement.normalized * m_MaximumSpeedAtGround;
            }

            m_playerRigidbody.velocity = movement;
        }

        /// <summary>
        /// Turn the player by the rotation axis
        /// </summary>
        /// <param name="desiredDirection"></param>
        private void TurnPlayer(Vector2 desiredDirection)
        {
            if (desiredDirection.sqrMagnitude <= 0.00001f)
                return;

            Vector3 lookDirection = new Vector3(desiredDirection.x, 0f, desiredDirection.y);
            lookDirection.Normalize();

            Quaternion newRotation = Quaternion.LookRotation(lookDirection);

            m_playerRigidbody.rotation = Quaternion.RotateTowards(m_playerRigidbody.rotation, newRotation, Time.fixedDeltaTime * m_MaximumTurnSpeed);
        }
    }
}
