﻿// <copyright file="PlayerAttackComponent.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>01/08/2019 16:57:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, Janunary 08, 2019

using System;
using System.Collections;
using UnityEngine;
using VuxtaStudio.Common.InputSystems;

namespace VuxtaStudio.TDS.PlayerControl
{
    /// <summary>
    /// Component that monitor for player attack inputs
    /// </summary>
    public class PlayerAttackComponent : PlayerControllableComponent
    {
        #region Private Fields

        private Vector2 m_LastPressedDirection = Vector2.zero;

        #endregion

        #region Public Fields

        /// <summary>
        /// Callback when attack button released
        /// </summary>
        public Action<Vector2> OnAttackButtonUp { get; set; }

        /// <summary>
        /// Callback when attack button is down
        /// </summary>
        public Action<Vector2> OnAttackButtonDown { get; set; }

        /// <summary>
        /// Callback when attack button is pressed
        /// </summary>
        public Action<Vector2> OnAttackButtonPressed { get; set; }

        /// <summary>
        /// Callback when attack button is down
        /// </summary>
        public Action OnReloadButtonDown { get; set; }


        /// <summary>
        /// Callback when attack button is down
        /// </summary>
        public Action OnSwitchButtonDown { get; set; }

        #endregion

        #region Unity MonoBehaviour Methods

        /// <summary>
        /// Check for attack button status and calling callbacks with pressed directions
        /// </summary>
        private void FixedUpdate()
        {
            if (m_InputSystem == null)
                return;

            if (!m_ControlEnable)
                return;

            if (m_InputSystem.GetButtonDown(PlayerInputID.Reload))
            {
                if (OnReloadButtonDown != null)
                    OnReloadButtonDown.Invoke();
            }

            if (m_InputSystem.GetButtonDown(PlayerInputID.SwitchWeapon))
            {
                if (OnSwitchButtonDown != null)
                    OnSwitchButtonDown.Invoke();
            }

            if (m_InputSystem.GetButtonUp(PlayerInputID.Attack))
            {
                if (OnAttackButtonUp != null)
                    OnAttackButtonUp.Invoke(m_LastPressedDirection);
            }

            if (m_InputSystem.GetButtonDown(PlayerInputID.Attack))
            {
                m_LastPressedDirection = m_InputSystem.GetXYAxis(PlayerInputID.AimAxis);

                if (OnAttackButtonDown != null)
                    OnAttackButtonDown.Invoke(m_LastPressedDirection);
            }

            if (m_InputSystem.GetButtonPressed(PlayerInputID.Attack))
            {
                m_LastPressedDirection = m_InputSystem.GetXYAxis(PlayerInputID.AimAxis);

                if (OnAttackButtonPressed != null)
                    OnAttackButtonPressed.Invoke(m_LastPressedDirection);
            }
        }

        #endregion
    }
}
