﻿// <copyright file="PlayerControllableComponent.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>01/08/2019 16:57:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, Janunary 08, 2019

using System.Collections;
using UnityEngine;
using VuxtaStudio.Common.InputSystems;

namespace VuxtaStudio.TDS.PlayerControl
{
    /// <summary>
    /// Base class of player controllable components
    /// Responsible for assign input system or user to control specific component functions
    /// Also allow enable/disable control of specific component
    /// </summary>
    public class PlayerControllableComponent : MonoBehaviour
    {
        #region Protected Fields

        protected InputSystem m_InputSystem;

        protected bool m_ControlEnable = true;
        protected IEnumerator m_ResetControlCoroutine;

        #endregion

        #region Public Methods

        /// <summary>
        /// Assign specific user ID to the control component
        /// Control component will try to get input system from InputSystemManager if there is one
        /// </summary>
        /// <param name="userID">User ID</param>
        public void AssignUser(int userID)
        {
            if (InputSystemManager.Instance.GetUser(userID, out m_InputSystem) == false)
            {
                Debug.LogError("[PlayerControlComponent::AssignUser] assign user control failed");
            }
        }

        /// <summary>
        /// Assign input system to the controllable component
        /// </summary>
        /// <param name="inputSystem">Input system</param>
        public void AssignInputSystem(InputSystem inputSystem)
        {
            this.m_InputSystem = inputSystem;
        }

        /// <summary>
        /// Enable user control to this component
        /// </summary>
        public void EnableControl()
        {
            if (m_ResetControlCoroutine != null)
                StopCoroutine(m_ResetControlCoroutine);

            m_ControlEnable = true;
        }

        /// <summary>
        /// Disable control to this component for certain time
        /// If duration is less than 0, it will be disable without re-enable again
        /// </summary>
        /// <param name="duration">Duration for disable, if it's less than 0, it means disable without re-enable again</param>
        public void DisableControl(float duration = -1f)
        {
            if (m_ResetControlCoroutine != null)
                StopCoroutine(m_ResetControlCoroutine);

            m_ControlEnable = false;

            if (duration > 0)
            {
                m_ResetControlCoroutine = ResetControlCroutine(duration);
                StartCoroutine(m_ResetControlCoroutine);
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Coroutine to re-enable control flags
        /// </summary>
        /// <param name="duration">Duration until re-enable</param>
        /// <returns>Wait for certain time peroids</returns>
        private IEnumerator ResetControlCroutine(float duration)
        {
            yield return new WaitForSecondsRealtime(duration);
            m_ControlEnable = true;
        }

        #endregion
    }
}
