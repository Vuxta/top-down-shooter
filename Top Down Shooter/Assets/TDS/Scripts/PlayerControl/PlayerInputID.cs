﻿using UnityEngine;

namespace VuxtaStudio.TDS.PlayerControl
{
    /// <summary>
    /// Player input control IDs
    /// </summary>
    public static class PlayerInputID
    {
        /// <summary>
        /// Move axis on XY plane
        /// </summary>
        public const int DirectionAxis = 0;

        /// <summary>
        /// Aim axis on XY plane
        /// </summary>
        public const int AimAxis = 1;

        public const int Attack = 2;

        public const int Reload = 3;

        public const int SwitchWeapon = 4;
    }
}
