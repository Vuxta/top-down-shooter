﻿// <copyright file="DefaultInputProfile.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>12/19/2018 17:08:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, December 19, 2018

using UnityEngine;
using VuxtaStudio.Common.InputSystems;

namespace VuxtaStudio.TDS.PlayerControl
{
    /// <summary>
    /// Default input profile for keyboard input
    /// Axis: WSAD
    /// Jump: Space
    /// </summary>
    [System.Serializable]
    public class DefaultInputProfile : InputSystemProfile
    {
        #region Constrcutors

        /// <summary>
        /// Construct basic keyboard input profile
        /// </summary>
        public DefaultInputProfile(VirtualJoystickAxisComponent virtualDirectionAxisHandler = null,
                                    VirtualJoystickAxisComponent virtualAimAxisHandler = null,
                                    VirtualButtonComponent virtualAttackButtonHandler = null,
                                    VirtualButtonComponent virtualReloadButtonHandler = null,
                                    VirtualButtonComponent virtualSwitchButtonHandler = null)
        {
            SimpleButtonAxisHandler directionAxisHandler = new SimpleButtonAxisHandler(new KeyboardButtonHandler(KeyCode.D),
                                                                                    new KeyboardButtonHandler(KeyCode.A),
                                                                                    new KeyboardButtonHandler(KeyCode.W),
                                                                                    new KeyboardButtonHandler(KeyCode.S));

            RegisterAxisHandler(PlayerInputID.DirectionAxis, directionAxisHandler, virtualDirectionAxisHandler);
            RegisterAxisHandler(PlayerInputID.AimAxis, virtualAimAxisHandler);
            RegisterButtonHandler(PlayerInputID.Attack, virtualAttackButtonHandler);
            RegisterButtonHandler(PlayerInputID.Reload, new KeyboardButtonHandler(KeyCode.R), virtualReloadButtonHandler);
            RegisterButtonHandler(PlayerInputID.SwitchWeapon, new KeyboardButtonHandler(KeyCode.Q), virtualSwitchButtonHandler);
        }

        #endregion
    }
}
