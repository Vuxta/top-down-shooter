﻿using UnityEngine;
using VuxtaStudio.Common.InputSystems;

namespace VuxtaStudio.TDS.PlayerControl
{
    /// <summary>
    /// Player input manager controller class
    /// </summary>
    public class PlayerController : MonoBehaviour
    {
        #region Private Fields

        private InputSystem m_InputSystem;
        private PlayerMovementComponent m_MovementComponent;
        private PlayerAttackComponent m_AttackComponent;

        #endregion

        /// <summary>
        /// Assign input system to controller, perphirial component will be updated with input system assigned as well
        /// </summary>
        /// <param name="inputSystem">Input system</param>
        /// <returns>Return true if set success</returns>
        public bool SetInputSystem(InputSystem inputSystem)
        {
            m_InputSystem = inputSystem;

            if (m_MovementComponent != null)
            {
                m_MovementComponent.AssignInputSystem(m_InputSystem);
            }

            if (m_AttackComponent != null)
            {
                m_AttackComponent.AssignInputSystem(m_InputSystem);
            }

            return true;
        }

        /// <summary>
        /// Assign movement component, could be used for different type of movement requirement, such as vehicle... etc
        /// </summary>
        /// <param name="movementComponent"></param>
        /// <returns></returns>
        public bool SetMovementComponent(PlayerMovementComponent movementComponent)
        {
            if (m_MovementComponent != null)
            {
                m_MovementComponent.DisableControl();
            }

            m_MovementComponent = movementComponent;
            m_MovementComponent.AssignInputSystem(m_InputSystem);

            return true;
        }

        /// <summary>
        /// Assign attack input component
        /// </summary>
        /// <param name="attackComponent"></param>
        /// <returns></returns>
        public bool SetAttackComponent(PlayerAttackComponent attackComponent)
        {
            if (m_AttackComponent != null)
            {
                m_AttackComponent.DisableControl();
            }

            m_AttackComponent = attackComponent;
            m_AttackComponent.AssignInputSystem(m_InputSystem);

            return true;
        }
    }
}
