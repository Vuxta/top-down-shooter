﻿using UnityEngine;

namespace VuxtaStudio.TDS.PlayerControl
{
    /// <summary>
    /// Movement component for player as humanoid
    /// </summary>
    public class PlayerHumanMovementComponent : PlayerRigidbodyMovementComponent
    {
    }
}
