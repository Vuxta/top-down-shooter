﻿using VuxtaStudio.Common.BitmaskActions;

namespace VuxtaStudio.TDS.SteeringSystem
{
    public abstract class SteeringBehaviourBaseAction : BitmaskAction
    {
        protected SteeringBehaviour m_SteeringBehaviour;

        public SteeringBehaviourBaseAction(SteeringBehaviour behaviour)
        {
            m_SteeringBehaviour = behaviour;
        }
    }
}
