using UnityEngine;
using System.Collections;
using VuxtaStudio.Common.BitmaskActions;

namespace VuxtaStudio.TDS.SteeringSystem
{
    public class MoveAction : SteeringBehaviourBaseAction
    {
        protected override long ActionMask
        {
            get
            {
                return SteeringBehaviour.SystemTask.Move;
            }
        }

        protected override long SuspendingActions
        {
            get
            {
                return 0;
            }
        }

        protected override long BlockingActions
        {
            get
            {
                return 0;
            }
        }

        protected override long RequiredActions
        {
            get
            {
                return 0;
            }
        }

        protected override long TerminatedActions
        {
            get
            {
                return 0;
            }
        }

        protected override bool Retriggerable
        {
            get
            {
                return true;
            }
        }

        public MoveAction(SteeringBehaviour behaviour) : base(behaviour)
        {
        }

        public override void BeginAction()
        {
        }

        public override void EndAction()
        {
            m_SteeringBehaviour.CommandLayer.MoveCmd(Vector3.zero);
        }

        public override void FixedUpdateAction()
        {
            //Debug.Log("steeringForce: " + m_SteeringBehaviour.Attribute.steeringForce);


            //Debug
#if false
            {
                Vector3 force = m_SteeringBehaviour.Attribute.steeringForce;
                Debug.Log("force: " + force.x + ", " + force.y + ", " + force.z + " length: " + force.magnitude);
            }
#endif
            if (m_SteeringBehaviour.Attribute.SteeringForce.sqrMagnitude > 0.01f)
                m_SteeringBehaviour.CommandLayer.MoveWithForce(m_SteeringBehaviour.Attribute.SteeringForce);

            //TODO: move to end actions
            m_SteeringBehaviour.Attribute.PreviousPos = m_SteeringBehaviour.AgentController.LocomotionComp.GetTransform().position;
        }

        public override bool CanBeActivated()
        {
            return true;
        }
    }
}
