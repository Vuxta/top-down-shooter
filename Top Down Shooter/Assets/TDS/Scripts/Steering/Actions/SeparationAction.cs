using UnityEngine;
using System.Collections;
using VuxtaStudio.Common.BitmaskActions;

namespace VuxtaStudio.TDS.SteeringSystem
{
    public class SeparationAction : SteeringBehaviourBaseAction
    {
        protected override long ActionMask
        {
            get
            {
                return SteeringBehaviour.SystemTask.Separation;
            }
        }

        protected override long SuspendingActions
        {
            get
            {
                return 0;
            }
        }

        protected override long BlockingActions
        {
            get
            {
                return 0;
            }
        }

        protected override long RequiredActions
        {
            get
            {
                return 0;
            }
        }

        protected override long TerminatedActions
        {
            get
            {
                return 0;
            }
        }

        protected override bool Retriggerable
        {
            get
            {
                return true;
            }
        }

        public SeparationAction(SteeringBehaviour behaviour) : base(behaviour)
        {
        }

        public override void BeginAction()
        {
        }

        public override void EndAction()
        {
        }

        public override void FixedUpdateAction()
        {
            //m_SteeringBehaviour.Attribute.steeringForce += m_SteeringBehaviour.SeparationForce();

            //TODO: edge cases should only handle when external forces comes in such as seperation force
            //hence, we should handle all the force at end action ?

            Vector3 edgeForce = Vector3.zero;

            if (m_SteeringBehaviour.IsEnableEdgeAvoidance)
                edgeForce = m_SteeringBehaviour.GetEdgeAvoidanceForce();

            m_SteeringBehaviour.Attribute.SteeringForce += m_SteeringBehaviour.GetAvoidanceForce();
            m_SteeringBehaviour.Attribute.SteeringForce += edgeForce;
        }

        public override bool CanBeActivated()
        {
            return true;
        }
    }
}
