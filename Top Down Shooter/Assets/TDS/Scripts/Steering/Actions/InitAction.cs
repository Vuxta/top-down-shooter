using UnityEngine;
using System.Collections;
using VuxtaStudio.Common.BitmaskActions;

namespace VuxtaStudio.TDS.SteeringSystem
{
    public class InitAction : SteeringBehaviourBaseAction
    {
        protected override long ActionMask
        {
            get
            {
                return SteeringBehaviour.SystemTask.Init;
            }
        }

        protected override long SuspendingActions
        {
            get
            {
                return 0;
            }
        }

        protected override long BlockingActions
        {
            get
            {
                return 0;
            }
        }

        protected override long RequiredActions
        {
            get
            {
                return 0;
            }
        }

        protected override long TerminatedActions
        {
            get
            {
                return 0;
            }
        }

        protected override bool Retriggerable
        {
            get
            {
                return true;
            }
        }

        public InitAction(SteeringBehaviour behaviour) : base(behaviour)
        {
        }

        public override void BeginAction()
        {
        }

        public override void EndAction()
        {
        }

        public override void FixedUpdateAction()
        {
            m_SteeringBehaviour.EnforceNonPenetration();

            m_SteeringBehaviour.Attribute.SteeringForce = Vector3.zero;

            if (m_SteeringBehaviour.IsEnableSeparation
                && m_SteeringBehaviour.NeighborComp.Neighbors.Count > 0)
            {
                if ((m_ActionManager.GetCurrentMasks() & SteeringBehaviour.SystemTask.Separation)
                    != SteeringBehaviour.SystemTask.Separation)
                {
                    //Debug.Log("Activate seperation force");
                    m_ActionManager.TryActivateAction(SteeringBehaviour.SystemTask.Separation);
                }
            }
            else
            {
                if ((m_ActionManager.GetCurrentMasks() & SteeringBehaviour.SystemTask.Separation)
                    == SteeringBehaviour.SystemTask.Separation)
                {
                    //Debug.Log("Deactive seperation force");
                    m_ActionManager.DeactivateAction(SteeringBehaviour.SystemTask.Separation);
                }
            }
        }

        public override bool CanBeActivated()
        {
            return true;
        }
    }
}
