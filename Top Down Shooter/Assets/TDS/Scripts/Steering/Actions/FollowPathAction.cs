//#define ENABLE_NAV_UPDATE_REPATH

using UnityEngine;
using System.Collections;
using VuxtaStudio.Common.BitmaskActions;
using VuxtaStudio.Common;

#if ENABLE_NAV_UPDATE_REPATH
using System;
#endif

namespace VuxtaStudio.TDS.SteeringSystem
{
    public class FollowPathAction : SteeringBehaviourBaseAction
    {

        protected override long ActionMask
        {
            get
            {
                return SteeringBehaviour.SystemTask.FollowPath;
            }
        }

        protected override long SuspendingActions
        {
            get
            {
                return 0;
            }
        }

        protected override long BlockingActions
        {
            get
            {
                return 0;
            }
        }

        protected override long RequiredActions
        {
            get
            {
                return 0;
            }
        }

        protected override long TerminatedActions
        {
            get
            {
                return 0;
            }
        }

        protected override bool Retriggerable
        {
            get
            {
                return true;
            }
        }

        private float m_TimeExpectedToWaypoint = 0f;
        private float m_TimeMargin = 1f;

        public FollowPathAction(SteeringBehaviour behaviour) : base(behaviour)
        {
        }

        public override void BeginAction()
        {
            m_TimeExpectedToWaypoint = Time.time + GetExpectedTime(m_SteeringBehaviour.SteerPath.TargetWaypoint) + m_TimeMargin;

#if ENABLE_NAV_UPDATE_REPATH
            EventCenter.StartListenToEvent<Akira.Events.NavMeshUpdateEvent>(OnNavMeshUpdate);
#endif
        }

        public override void EndAction()
        {
#if ENABLE_NAV_UPDATE_REPATH
            EventCenter.StopListenToEvent<Akira.Events.NavMeshUpdateEvent>(OnNavMeshUpdate);
#endif
        }

        public override void FixedUpdateAction()
        {
            Vector3 targetWaypoint = m_SteeringBehaviour.SteerPath.TargetWaypoint;
            float toTargetDistance = Vector3.Distance(targetWaypoint, m_SteeringBehaviour.AgentController.LocomotionComp.GetTransform().position);

            //Debug.Log("CurrentWaypoint: " + targetWaypoint);

            //check if is closed to destination
            if (m_SteeringBehaviour.SteerPath.IsFinished()
                && toTargetDistance < m_SteeringBehaviour.StoppingDistance)
            {
                //TODO: ?
                //it might be possible that it's getting push away
                if ((m_ActionManager.GetCurrentMasks() & SteeringBehaviour.SystemTask.Move)
                    == SteeringBehaviour.SystemTask.Move)
                {
                    //Debug.Log("Follow path complete, stop update force for following path actions");
                    m_ActionManager.DeactivateAction(SteeringBehaviour.SystemTask.Move);
                }

                return;
            }
            else
            {
                if ((m_ActionManager.GetCurrentMasks() & SteeringBehaviour.SystemTask.Move)
                    != SteeringBehaviour.SystemTask.Move)
                {
                    //Debug.Log("Activate move actions");
                    m_ActionManager.TryActivateAction(SteeringBehaviour.SystemTask.Move);
                }
            }

            if (toTargetDistance < m_SteeringBehaviour.WaypointSeekDistance || IsPassed(targetWaypoint))
            {
                m_SteeringBehaviour.SteerPath.SetNextWaypoint();
                targetWaypoint = m_SteeringBehaviour.SteerPath.TargetWaypoint;
                m_TimeExpectedToWaypoint = Time.time + GetExpectedTime(m_SteeringBehaviour.SteerPath.TargetWaypoint) + m_TimeMargin;
            }

            //exceed expected, recalculate path again
            if (Time.time > m_TimeExpectedToWaypoint && m_SteeringBehaviour.SteerPath.IsFinished() == false)
            {
                //Debug.Log("FollowPath exceed path time, recalculate path: " + m_SteeringBehaviour.AgentInfo.id);
                m_SteeringBehaviour.SteerPath.CalculatePathAsync(m_SteeringBehaviour.AgentController.LocomotionComp.GetTransform().position, m_SteeringBehaviour.SteerPath.Destination);
                targetWaypoint = m_SteeringBehaviour.SteerPath.TargetWaypoint;
                m_TimeExpectedToWaypoint = Time.time + GetExpectedTime(m_SteeringBehaviour.SteerPath.TargetWaypoint) + m_TimeMargin;
            }

            if (m_SteeringBehaviour.SteerPath.IsFinished() == false)
            {
                m_SteeringBehaviour.Attribute.SteeringForce
                    += m_SteeringBehaviour.SeekForce(targetWaypoint);
            }
            else
            {
                /*
                m_SteeringBehaviour.Attribute.steeringForce
                    += m_SteeringBehaviour.ArriveForce(targetWaypoint, 5f);
                    */

                m_SteeringBehaviour.Attribute.SteeringForce
                    += m_SteeringBehaviour.SeekForce(targetWaypoint);
            }
        }

        public override bool CanBeActivated()
        {
            return true;
        }

        private bool IsPassed(Vector3 targetPos)
        {
            Vector3 targetFromPrevious = targetPos - m_SteeringBehaviour.Attribute.PreviousPos;
            Vector3 targetFromNow = targetPos - m_SteeringBehaviour.AgentController.LocomotionComp.GetTransform().position;

            float dotVal = Vector3.Dot(targetFromNow, targetFromPrevious);

            if (dotVal <= 0)
                return true;
            else
                return false;
        }

        private float GetExpectedTime(Vector3 targetPos)
        {
            Vector3 toTarget = targetPos - m_SteeringBehaviour.AgentController.LocomotionComp.GetTransform().position;
            toTarget.y = 0f;

            return toTarget.magnitude / m_SteeringBehaviour.LocomotionComp.MaxSpeed;
        }

#if ENABLE_NAV_UPDATE_REPATH
        void OnNavMeshUpdate(Akira.Events.NavMeshUpdateEvent navMeshUpdateEvent)
        {
            Debug.Log("OnNavMeshUpdate");
            Repath();
        }

        void Repath()
        {
            m_SteeringBehaviour.SteerPath.CalculatePathAsync(m_SteeringBehaviour.AgentController.LocomotionComp.GetTransform().position, m_SteeringBehaviour.SteerPath.Destination);
            timeExpectedToWaypoint = Time.time + GetExpectedTime(m_SteeringBehaviour.SteerPath.TargetWaypoint) + timeMargin;
        }
#endif
    }
}
