using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using VuxtaStudio.Common.BitmaskActions;

namespace VuxtaStudio.TDS.SteeringSystem
{
    [RequireComponent(typeof(SteeringController))]
    public class SteeringBehaviour : MonoBehaviour
    {

        public static class SystemTask
        {
            //please follow the bitwise rules so that we will be able to run multiple effect at the same time
            //every effect can only be defined as one task
            public const long None = 0;
            public const long Init = 1 << 0;
            public const long Separation = 1 << 1;
            public const long FollowPath = 1 << 2;
            public const long Move = 1 << 3;
        }

        enum TaskExecutionOrder
        {
            Init,
            Separation,
            FollowPath,
            Move,
            Size
        }

        private SteeringController m_AgentController;
        public SteeringController AgentController
        {
            get
            {
                return m_AgentController;
            }
        }

        public CommandLayer CommandLayer
        {
            get
            {
                return AgentController.CommandLayer;
            }
        }

        public LocomotionComponent LocomotionComp
        {
            get
            {
                return AgentController.LocomotionComp;
            }
        }

        public NeighborTagComponent NeighborComp
        {
            get
            {
                return AgentController.NeighborComp;
            }
        }

        [SerializeField]
        private float m_WaypointSeekDistance = 0.01f;
        public float WaypointSeekDistance
        {
            get
            {
                return m_WaypointSeekDistance;
            }
        }

        [SerializeField]
        private float m_StoppingDistance = 0.35f;
        public float StoppingDistance
        {
            get
            {
                return m_StoppingDistance;
            }
        }

        [SerializeField]
        private bool m_IsEnableSeparation = true;
        public bool IsEnableSeparation
        {
            get
            {
                return m_IsEnableSeparation;
            }
        }

        [SerializeField]
        private float m_AheadDistance = 1.5f;

        [SerializeField]
        private float m_AvoidanceRadius = 1.5f;
        public float AvoidanceRadius
        {
            get
            {
                return m_AvoidanceRadius;
            }
        }


        [SerializeField]
        private float m_MaxAvoidanceForce = 5f;
        [SerializeField]
        private float m_MinAvoidanceForce = 2f;

        [SerializeField]
        private float m_BaseRadius = 0.5f;
        public float BaseRadius
        {
            get
            {
                return m_BaseRadius;
            }
        }

        [SerializeField]
        private bool m_IsEnableNoPenetration = true;
        public bool IsEnableNoPenetration
        {
            get
            {
                return m_IsEnableNoPenetration;
            }
        }

        [SerializeField]
        private float m_EdgeRadius = 1f;

        [SerializeField]
        private float m_MaxEdgeAvoidanceForce = 5f;
        [SerializeField]
        private float m_MinEdgeAvoidanceForce = 2f;

        [SerializeField]
        private bool m_IsEnableEdgeAvoidance = false;
        public bool IsEnableEdgeAvoidance
        {
            get
            {
                return m_IsEnableEdgeAvoidance;
            }
        }

        private SteeringAttribute m_Attribute = new SteeringAttribute();
        public SteeringAttribute Attribute
        {
            get
            {
                return m_Attribute;
            }
        }

        private SteeringPath m_SteerPath = new SteeringPath();
        public SteeringPath SteerPath
        {
            get
            {
                return m_SteerPath;
            }
        }

        private BitmaskActionMachine m_ActionMachine;
        private BitmaskAction[] m_BitmaskActions;

        private InitAction m_InitAction;
        private SeparationAction m_SeperateAction;
        private FollowPathAction m_FollowPathAction;
        private MoveAction m_MoveAction;

        private void Awake()
        {
            m_AgentController = GetComponent<SteeringController>();

            m_InitAction = new InitAction(this);
            m_SeperateAction = new SeparationAction(this);
            m_FollowPathAction = new FollowPathAction(this);
            m_MoveAction = new MoveAction(this);

            m_BitmaskActions = new BitmaskAction[] { m_InitAction,
                                                    m_SeperateAction,
                                                    m_FollowPathAction,
                                                    m_MoveAction};


            m_ActionMachine = new BitmaskActionMachine(m_BitmaskActions);
            m_ActionMachine.TryActivateAction(SystemTask.Init);
        }

        private void OnEnable()
        {
            if (AgentController == null || AgentController.LocomotionComp == null)
            {
                Attribute.PreviousPos = this.transform.position;
            }
            else
            {
                Attribute.PreviousPos = AgentController.LocomotionComp.GetTransform().position;
            }
        }

        private void FixedUpdate()
        {
            if (m_ActionMachine != null)
                m_ActionMachine.OnMachineFixedUpdate();
        }

        #region seek_force
        public Vector3 SeekForce(Vector3 targetPos)
        {
            Vector3 desiredVelocity = targetPos - AgentController.LocomotionComp.GetTransform().position;
            desiredVelocity.y = 0; //make only 2d vector
            desiredVelocity = desiredVelocity.normalized * LocomotionComp.MaxSpeed;

            //Debug.Log("seek desiredVelocity: " + desiredVelocity);

            return (desiredVelocity - LocomotionComp.GetTargetVelocity());
        }
        #endregion seek_force

        #region arrive_force
        public Vector3 ArriveForce(Vector3 targetPos, float decelration)
        {
            Vector3 toTarget = targetPos - AgentController.LocomotionComp.GetTransform().position;
            toTarget.y = 0f; //again calculate only 2D
            float dist = toTarget.magnitude;

            if (dist > 0)
            {
                float speed = dist / decelration;
                speed = Mathf.Min(speed, LocomotionComp.MaxSpeed);
                Vector3 desiredVelocity = toTarget * speed / dist;

                //Debug.Log("arrive desiredVelocity: " + desiredVelocity);

                return desiredVelocity - LocomotionComp.GetTargetVelocity();
            }

            return Vector3.zero;
        }
        #endregion arrive_force

        #region separation_force
        public Vector3 SeparationForce()
        {
            Vector3 force = Vector3.zero;

            if (NeighborComp != null && NeighborComp.Neighbors.Count > 0)
            {
                for (int i = 0; i < NeighborComp.Neighbors.Count; i++)
                {
                    Vector3 toAgent = AgentController.LocomotionComp.GetTransform().position - NeighborComp.Neighbors[i].AgentController.LocomotionComp.GetTransform().position;
                    toAgent.y = 0f; //only consider 2d case

                    force += toAgent.normalized / toAgent.magnitude;
                }
            }

            return force;
        }
        #endregion separation_force

        #region avoidance_force

        //avoidance force
        public Vector3 GetAvoidanceForce()
        {
            Vector3 aheadDir = LocomotionComp.GetTargetVelocity().normalized * m_AheadDistance;
            Vector3 aheadVec = AgentController.LocomotionComp.GetTransform().position + aheadDir;
            Vector3 aheadVecHalf = AgentController.LocomotionComp.GetTransform().position + aheadDir * 0.5f;
            SteeringBehaviour referenceAgent = null;
            Vector3 avoidanceForce = Vector3.zero;
            Vector3 dirFromRefAgent = Vector3.zero;

            if (GetReferenceAgent(AgentController.LocomotionComp.GetTransform().position, aheadVec, aheadVecHalf, out referenceAgent))
            {
                float distance = 0f;
                float referenceAgentRadius = 0f;
                float forceVal = 0f;

                //sideways repulsion method
                {
                    dirFromRefAgent = this.transform.position - referenceAgent.LocomotionComp.GetTransform().position;
                    dirFromRefAgent.y = 0f;

                    if (referenceAgent.LocomotionComp.GetTargetVelocity() != Vector3.zero)
                    {
                        avoidanceForce = dirFromRefAgent - Vector3.Project(dirFromRefAgent, referenceAgent.LocomotionComp.GetTargetVelocity());
                    }
                    else //deal with when reference agent's velocity is zero, we need another way calculating the side force
                    {
                        avoidanceForce = dirFromRefAgent + Vector3.Project(-1f * dirFromRefAgent, LocomotionComp.GetTargetVelocity());

                        //Debug.Log("reference vel is 0, self get avoidance force");
                    }

                    //Debug.Log("dirFromAgent: " + dirFromRefAgent + " avoidanceForce: " + avoidanceForce + "referenceAgentVel: " + referenceAgent.MoveComp.TargetVelocity);
                    distance = dirFromRefAgent.magnitude;
                    referenceAgentRadius = referenceAgent.AvoidanceRadius;
                    forceVal = m_MinAvoidanceForce + (m_MaxAvoidanceForce - m_MinAvoidanceForce) * ((referenceAgentRadius - distance) / referenceAgentRadius);
                    forceVal = Mathf.Clamp(forceVal, m_MinAvoidanceForce, m_MaxAvoidanceForce);
                }

                //Debug.Log("forceVal: " + forceVal);

                avoidanceForce = avoidanceForce.normalized * forceVal;

                Debug.DrawLine(referenceAgent.AgentController.LocomotionComp.GetTransform().position, referenceAgent.AgentController.LocomotionComp.GetTransform().position + referenceAgent.LocomotionComp.GetTargetVelocity() * 2f, Color.blue);
                Debug.DrawLine(AgentController.LocomotionComp.GetTransform().position, AgentController.LocomotionComp.GetTransform().position + avoidanceForce * 2f, Color.red);
            }
            else
            {
                avoidanceForce = Vector3.zero;
            }

            return avoidanceForce;
        }

        bool GetReferenceAgent(Vector3 currentPos, Vector3 aheadVec, Vector3 aheadVecHalf, out SteeringBehaviour referenceAgent)
        {
            referenceAgent = null;
            float shortestDistance = float.MaxValue;
            float currentDistance = 0f;
            bool isFound = false;

            for (int i = 0; i < NeighborComp.Neighbors.Count; i++)
            {
                Vector3 targetPos = NeighborComp.Neighbors[i].AgentController.LocomotionComp.GetTransform().position;
                bool isIntersect = IsLineIntersecsCircle(aheadVec, aheadVecHalf, targetPos);
                currentDistance = Vector3.Distance(currentPos, targetPos);
                if (isIntersect && currentDistance < shortestDistance)
                {
                    shortestDistance = currentDistance;
                    referenceAgent = NeighborComp.Neighbors[i];
                    isFound = true;
                }
            }

            return isFound;
        }

        bool IsLineIntersecsCircle(Vector3 aheadVec, Vector3 aheadVecHalf, Vector3 targetPos)
        {
            return Vector3.Distance(targetPos, aheadVec) <= m_AvoidanceRadius
                || Vector3.Distance(targetPos, aheadVecHalf) <= m_AvoidanceRadius;
        }

        #endregion avoidance_force

        #region edge_force
        public Vector3 GetEdgeAvoidanceForce()
        {
            if (LocomotionComp.GetTargetVelocity() == Vector3.zero)
                return Vector3.zero;

            Vector3 avoidanceForce = Vector3.zero;
            NavMeshHit hit;

            if (NavMesh.Raycast(AgentController.LocomotionComp.GetTransform().position, AgentController.LocomotionComp.GetTransform().position + LocomotionComp.GetTargetVelocity().normalized * m_EdgeRadius, out hit, NavMesh.AllAreas) == false)
                return Vector3.zero;

            if (hit.distance > m_EdgeRadius)
                return Vector3.zero;

            avoidanceForce = AgentController.LocomotionComp.GetTransform().position - hit.position;
            avoidanceForce.y = 0f; //yeah, again, calculate only 2D for force
            float dotValue = Vector3.Dot(avoidanceForce, LocomotionComp.GetTargetVelocity());

            if (dotValue >= 0f) //check if we are heading to the wall, return 0 if not
                return Vector3.zero;

            float forceVal = m_MinEdgeAvoidanceForce + (m_MaxEdgeAvoidanceForce - m_MinEdgeAvoidanceForce) * ((m_EdgeRadius - hit.distance) / m_EdgeRadius);
            forceVal = Mathf.Clamp(forceVal, m_MinAvoidanceForce, m_MaxAvoidanceForce);

            avoidanceForce = avoidanceForce.normalized * forceVal;

            Debug.Log("edge avoidance: " + avoidanceForce + " edge: " + hit.position + " dis: " + hit.distance);
            Debug.DrawLine(AgentController.LocomotionComp.GetTransform().position, AgentController.LocomotionComp.GetTransform().position + LocomotionComp.GetTargetVelocity() * 2f, Color.blue);
            Debug.DrawLine(AgentController.LocomotionComp.GetTransform().position, AgentController.LocomotionComp.GetTransform().position + avoidanceForce * 2f, Color.yellow);

            return avoidanceForce;
        }
        #endregion edge_force

        #region enforce_nonpen
        public void EnforceNonPenetration()
        {
            if (!m_IsEnableNoPenetration)
                return;

            Vector3 newPos = AgentController.LocomotionComp.GetTransform().position;
            if (NeighborComp != null && NeighborComp.Neighbors.Count > 0)
            {
                for (int i = 0; i < NeighborComp.Neighbors.Count; i++)
                {
                    Vector3 toAgent = AgentController.LocomotionComp.GetTransform().position - NeighborComp.Neighbors[i].AgentController.LocomotionComp.GetTransform().position;
                    toAgent.y = 0;
                    float distFromEachOther = toAgent.magnitude;

                    float amountOverlap = m_BaseRadius + NeighborComp.Neighbors[i].BaseRadius - distFromEachOther;
                    if (amountOverlap > 0)
                    {
                        newPos += (toAgent / distFromEachOther) * amountOverlap;
                    }
                }

                if (newPos != AgentController.LocomotionComp.GetTransform().position)
                {
                    AgentController.LocomotionComp.GetTransform().position = newPos;
                    //Debug.Log("EnforceNonePen, set newPos at: " + newPos);
                }
            }

            return;
        }
        #endregion enforce_nonpen

        #region external_apis
        public void SetDestination(Vector3 destination)
        {
            SteerPath.CalculatePathAsync(AgentController.LocomotionComp.GetTransform().position, destination);
            m_ActionMachine.TryActivateAction(SystemTask.FollowPath);
            m_ActionMachine.TryActivateAction(SystemTask.Move);
        }

        public void Stop()
        {
            m_ActionMachine.DeactivateAction(SystemTask.FollowPath);
            m_ActionMachine.DeactivateAction(SystemTask.Move);
        }
        #endregion external_apis
    }
}
