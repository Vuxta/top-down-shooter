using UnityEngine;
using System.Collections;

namespace VuxtaStudio.TDS.SteeringSystem
{
    [System.Serializable]
    public struct MoveCmdInfo
    {
        public int ActorID;
        public int commandIndex;
        public float time;
        public Vector3 position;
        public Vector3 velocity;
    }

    [RequireComponent(typeof(SteeringController))]
    public class CommandLayer : MonoBehaviour
    {
        private SteeringController m_Controller;

        private void Awake()
        {
            m_Controller = GetComponent<SteeringController>();
        }

        #region move_cmd

        public void MoveWithForce(Vector3 force)
        {
            Vector3 newVelocity = Vector3.ClampMagnitude(m_Controller.LocomotionComp.GetTargetVelocity() + force, m_Controller.LocomotionComp.MaxSpeed);
            MoveCmd(newVelocity);
        }

        public void MoveCmd(Vector3 vel)
        {
            if (vel == m_Controller.LocomotionComp.GetTargetVelocity())
            {
                //Debug.Log("input velocity not changed, abort");
                return;
            }

            //Debug
#if false
            {
                Vector3 diff = vel - controller.LocomotionComp.GetTargetVelocity();
                Debug.LogError("vel diff: " + diff.x + ", " + diff.y + ", " + diff.z + " length: " + diff.magnitude);
            }
#endif

            m_Controller.LocomotionComp.SetTargetVelocity(vel);
        }
        #endregion move_cmd

        #region destination_cmd
        public void SetDestination(Vector3 pos)
        {
            m_Controller.SteerBehaviour.SetDestination(pos);
        }

        public void Stop()
        {
            m_Controller.SteerBehaviour.Stop();
        }

        #endregion destination_cmd
    }
}
