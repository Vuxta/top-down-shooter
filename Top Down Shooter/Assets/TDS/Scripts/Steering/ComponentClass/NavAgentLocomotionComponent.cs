#define ENABLE_SMOOTHING

using UnityEngine;
using System.Collections;
using UnityEngine.AI;
using System.Collections.Generic;
using System.Linq;

namespace VuxtaStudio.TDS.SteeringSystem
{
    public struct SmoothMoveCmd
    {
        public Vector3 StartPos;
        public Vector3 Velocity;
        public Vector3 Heading;
        public float Magnitude;
    }

    //note that when using agent, all calculation should be set as 2D, which y will be 0
    [RequireComponent(typeof(NavMeshAgent))]
    public class NavAgentLocomotionComponent : LocomotionComponent
    {
        [SerializeField]
        private NavMeshAgent m_NavAgent;

        private Vector3 m_TargetVelocity = Vector3.zero;

        private SmoothMoveCmd m_SmoothCmd = new SmoothMoveCmd();
        private Queue<Vector3> m_HeadingList = new Queue<Vector3>();
        private Vector3 m_HeadingSum = Vector3.zero;
        private bool m_IsSmoothing = false;

        private void Awake()
        {
            if (m_NavAgent == null)
                m_NavAgent = GetComponent<NavMeshAgent>();
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            Vector3 heading = m_NavAgent.velocity.normalized;

            if (m_IsSmoothing)
            {
                heading = m_SmoothCmd.Heading;
            }

            if (m_HeadingList.Count < 5)
            {
                m_HeadingList.Enqueue(heading);
                m_HeadingSum += heading;
            }
            else
            {
                m_HeadingSum -= m_HeadingList.Dequeue();
                m_HeadingList.Enqueue(heading);
                m_HeadingSum += heading;
            }

            if (!m_IsSmoothing)
                m_NavAgent.velocity = m_TargetVelocity;
            else
                SmoothMoveUpdate();
        }

        #region smooth_movement

        private void SmoothMoveUpdate()
        {
            if (GetDistanceToSmoothTargetPath() < 0.01f)
            {
                m_IsSmoothing = false;

                if (m_SmoothCmd.Velocity != Vector3.zero)
                {
                    m_NavAgent.velocity = m_SmoothCmd.Velocity;
                }
                else //stop command ending
                {
                    m_NavAgent.transform.position = m_SmoothCmd.StartPos;
                    m_NavAgent.velocity = Vector3.zero;
                }
                Debug.Log("Smoothing complete");
                return;
            }

            Vector3 targetHeading = m_HeadingSum / m_HeadingList.Count;

            //TODO: how to handle stop command nicely
            if (m_SmoothCmd.Velocity != Vector3.zero)
            {
                m_NavAgent.velocity = targetHeading.normalized * m_SmoothCmd.Magnitude;
            }
            else
            {
                targetHeading = m_SmoothCmd.StartPos - m_NavAgent.transform.position;
                m_NavAgent.velocity = targetHeading.normalized * m_NavAgent.velocity.magnitude;
                Debug.Log("zero command smoothing: " + targetHeading + " vel: " + m_NavAgent.velocity + " way: " + m_SmoothCmd.Velocity);
            }
        }

        float GetDistanceToSmoothTargetPath()
        {
            float distance = Vector3.Cross(m_SmoothCmd.Velocity, m_NavAgent.transform.position - m_SmoothCmd.StartPos).magnitude;
            Debug.Log("dis: " + distance + " smoothVel: " + m_SmoothCmd.Velocity + " pos: " + m_NavAgent.transform.position + " startPos: " + m_SmoothCmd.StartPos);
            return distance;
        }
        #endregion smooth_movement

        public override void TurnTowards(Vector3 pos)
        {
            Vector3 dir = pos - this.transform.position;
            TurnToDirection(dir);
        }

        public override void TurnToDirection(Vector3 dir)
        {
            Vector3 lookDirection = dir;
            lookDirection.y = 0f;
            lookDirection.Normalize();

            Quaternion newRotation = Quaternion.LookRotation(lookDirection);

            this.transform.rotation = Quaternion.RotateTowards(transform.rotation, newRotation, Time.fixedDeltaTime * m_TurnSpeed);
        }

        public override void SetMovementForce(Vector3 force)
        {
            Vector3 newVelocity = Vector3.ClampMagnitude(m_TargetVelocity + force, MaxSpeed);
            SetTargetVelocity(newVelocity);
        }

        public override void SetTargetVelocity(Vector3 vec)
        {
            if (vec == m_TargetVelocity)
            {
                Debug.Log("input velocity not changed, abort");
                return;
            }

            m_IsSmoothing = false;
            m_TargetVelocity = vec;
        }

        public override Vector3 GetTargetVelocity()
        {
            return m_TargetVelocity;
        }

        public override Vector3 GetCurrentVelocity()
        {
            return m_NavAgent.velocity;
        }

        public override Transform GetTransform()
        {
            return m_NavAgent.transform;
        }

        public override void SetSmoothMovement(SmoothMoveCmd cmd)
        {
            m_IsSmoothing = true;
            m_SmoothCmd = cmd;
            m_TargetVelocity = cmd.Velocity;
        }
    }
}
