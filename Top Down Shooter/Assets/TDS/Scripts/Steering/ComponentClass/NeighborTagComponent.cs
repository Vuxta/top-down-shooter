using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VuxtaStudio.TDS.SteeringSystem
{
    //stores neighbors' info
    public class NeighborTagComponent : MonoBehaviour
    {
        private List<SteeringBehaviour> neighbors = new List<SteeringBehaviour>();

        public List<SteeringBehaviour> Neighbors
        {
            get
            {
                return neighbors;
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<SteeringBehaviour>() == null)
                return;

            neighbors.Add(other.GetComponent<SteeringBehaviour>());
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.GetComponent<SteeringBehaviour>() == null)
                return;

            neighbors.Remove(other.GetComponent<SteeringBehaviour>());
        }
    }
}
