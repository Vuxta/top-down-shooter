using UnityEngine;

namespace VuxtaStudio.TDS.SteeringSystem
{
    public abstract class LocomotionComponent : MonoBehaviour
    {
        [SerializeField]
        protected float m_MaxSpeed = 5f;
        public float MaxSpeed
        {
            get
            {
                return m_MaxSpeed;
            }
        }

        [SerializeField]
        protected float m_MinSmoothDistance = 0.01f;
        public float MinSmoothDistance
        {
            get
            {
                return m_MinSmoothDistance;
            }
        }

        [SerializeField]
        protected float m_MaxSmoothDistance = 0.1f;
        public float MaxSmoothDistance
        {
            get
            {
                return m_MaxSmoothDistance;
            }
        }

        [SerializeField]
        protected float m_TurnSpeed = 360f;
        public float TurnSpeed
        {
            get
            {
                return m_TurnSpeed;
            }
        }

        public abstract void SetMovementForce(Vector3 force);
        public abstract void SetTargetVelocity(Vector3 vec);
        public abstract void TurnTowards(Vector3 pos);
        public abstract void TurnToDirection(Vector3 dir);
        public abstract Vector3 GetTargetVelocity();
        public abstract Vector3 GetCurrentVelocity();
        public abstract Transform GetTransform();
        public abstract void SetSmoothMovement(SmoothMoveCmd cmd);
    }
}
