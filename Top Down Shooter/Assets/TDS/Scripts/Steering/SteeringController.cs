using UnityEngine;
using System.Collections;
using UnityEngine.AI;

namespace VuxtaStudio.TDS.SteeringSystem
{
    [RequireComponent(typeof(NeighborTagComponent))]
    [RequireComponent(typeof(LocomotionComponent))]
    [RequireComponent(typeof(SteeringBehaviour))]
    [RequireComponent(typeof(CommandLayer))]
    public class SteeringController : MonoBehaviour
    {
        private NeighborTagComponent m_NeighborComp;
        public NeighborTagComponent NeighborComp
        {
            get
            {
                return m_NeighborComp;
            }
        }

        private LocomotionComponent m_LocomotionComp;
        public LocomotionComponent LocomotionComp
        {
            get
            {
                return m_LocomotionComp;
            }
        }

        private SteeringBehaviour m_SteerBehaviour;
        public SteeringBehaviour SteerBehaviour
        {
            get
            {
                return m_SteerBehaviour;
            }
        }

        private CommandLayer m_CommandLayer;
        public CommandLayer CommandLayer
        {
            get
            {
                return m_CommandLayer;
            }
        }

        public SteeringPath SteerPath
        {
            get
            {
                return SteerBehaviour.SteerPath;
            }
        }

        private void Awake()
        {
            m_NeighborComp = GetComponent<NeighborTagComponent>();
            m_LocomotionComp = GetComponent<LocomotionComponent>();
            m_SteerBehaviour = GetComponent<SteeringBehaviour>();
            m_CommandLayer = GetComponent<CommandLayer>();
        }
    }
}
