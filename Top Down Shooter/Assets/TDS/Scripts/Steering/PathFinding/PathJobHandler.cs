using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using VuxtaStudio.Common;

namespace VuxtaStudio.TDS.SteeringSystem
{
    public class PathJobHandler : MonoSingleton<PathJobHandler>
    {
        [SerializeField]
        private int m_NumberOfCyclesPerUpdate = 5;

        private LinkedList<PathJob> m_JobList = new LinkedList<PathJob>();
        private LinkedListNode<PathJob> m_JobNode = null;

        // Update is called once per frame
        void Update()
        {
            if (m_JobList.Count <= 0)
                return;

            int cyclesRemaining = m_NumberOfCyclesPerUpdate;
            m_JobNode = m_JobList.First;
            while (m_JobList.Count > 0
                && cyclesRemaining > 0
                && m_JobNode != null)
            {
                m_JobNode.Value.Execute();

                //advance to next
                m_JobNode = m_JobNode.Next;
                cyclesRemaining--;
                m_JobList.RemoveFirst();
            }
        }

        public void Register(PathJob job)
        {
            m_JobList.AddLast(job);
        }

        public void UnRegister(PathJob job)
        {
            m_JobList.Remove(job);
        }
    }
}
