using UnityEngine;
using System.Collections;

namespace VuxtaStudio.TDS.SteeringSystem
{
    public class CalculatePathJob : PathJob
    {
        private SteeringPath m_PathInfo;
        private Vector3 m_SourcePos;
        private Vector3 m_TargetPos;

        public CalculatePathJob(SteeringPath pathInfo, Vector3 sourcePos, Vector3 targetPos)
        {
            this.m_PathInfo = pathInfo;
            this.m_SourcePos = sourcePos;
            this.m_TargetPos = targetPos;
        }

        public override void Execute()
        {
            m_PathInfo.CalculatePath(m_SourcePos, m_TargetPos);
        }
    }
}
