//#define EXPORT_PATH_CORNERS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;

#if EXPORT_PATH_CORNERS
using System.Linq;
#endif

namespace VuxtaStudio.TDS.SteeringSystem
{
    public class SteeringPath
    {
        private LinkedList<Vector3> m_Waypoints = new LinkedList<Vector3>();
        private LinkedListNode<Vector3> m_TargetWaypointNode = null;
        private NavMeshPath m_NavPath;
        private bool m_IsLoopPath = false;

#if EXPORT_PATH_CORNERS
        private Vector3[] corners = null;

        public Vector3[] Corners
        {
            get
            {
                return corners;
            }
        }
#endif

        public Vector3 TargetWaypoint
        {
            get
            {
                if (m_TargetWaypointNode != null)
                    return m_TargetWaypointNode.Value;
                else
                {
                    Debug.LogError("[SteeringPath] TargetWaypoint is null, return 0");
                    return Vector3.zero;
                }
            }
        }

        public Vector3 Destination
        {
            get
            {
                if (m_Waypoints.Count > 0)
                    return m_Waypoints.Last.Value;
                else
                {
                    Debug.LogError("[SteeringPath] waypoints is null, return 0");
                    return Vector3.zero;
                }
            }
        }

        public void SetNextWaypoint()
        {
            if (m_Waypoints.Count <= 0)
            {
                Debug.LogError("[SteeringPath] SetNextWaypoint error, no waypoints exist");
                return;
            }

            if (m_IsLoopPath && m_TargetWaypointNode == m_Waypoints.Last)
            {
                m_TargetWaypointNode = m_Waypoints.First;
            }
            else
            {
                if (m_TargetWaypointNode.Next != null)
                    m_TargetWaypointNode = m_TargetWaypointNode.Next; //what about next is null ?
                else
                    Debug.Log("next waypoint is null, abort...");
            }

            //if (targetWaypointNode != null)
            //Debug.Log("Go to next waypoint: " + targetWaypointNode.Value);
        }

        public void CalculatePathAsync(Vector3 sourcePosition, Vector3 targetPosition)
        {
            if (PathJobHandler.Instance == null)
            {
                CalculatePath(sourcePosition, targetPosition);
                Debug.LogWarning("[SteeringPath] Missing job handler, calling simple method instead");
                return;
            }

            //init source and destination first
            Vector3[] initPos = new Vector3[2] { sourcePosition, targetPosition };
            m_Waypoints.Clear();
            m_Waypoints = new LinkedList<Vector3>(initPos);
            m_TargetWaypointNode = m_Waypoints.First;

            CalculatePathJob newJob = new CalculatePathJob(this, sourcePosition, targetPosition);
            PathJobHandler.Instance.Register(newJob);
        }

        public void CalculatePath(Vector3 sourcePosition, Vector3 targetPosition)
        {
            m_NavPath = new NavMeshPath();
            NavMesh.CalculatePath(sourcePosition, targetPosition, NavMesh.AllAreas, m_NavPath);
            m_Waypoints.Clear();
            m_Waypoints = new LinkedList<Vector3>(m_NavPath.corners);
            m_TargetWaypointNode = m_Waypoints.First;

#if EXPORT_PATH_CORNERS
            corners = waypoints.ToArray<Vector3>();
#endif
        }

        public bool IsFinished()
        {
            if (m_TargetWaypointNode != m_Waypoints.Last)
                return false;
            else
                return true;
        }

        public void Clear()
        {
            m_Waypoints.Clear();
        }
#if VUXTASTUDIO_DEBUG
        public void DebugDraw()
        {
            //debug
            if (waypoints.Count > 1)
            {
                LinkedListNode<Vector3> traverseNode = null;
                traverseNode = waypoints.First;

                for (int i = 0; i < waypoints.Count - 1; i++)
                {
                    Debug.DrawLine(traverseNode.Value, traverseNode.Next.Value, Color.red);

                    if (traverseNode != targetWaypointNode)
                        Debug.DrawRay(traverseNode.Value, Vector3.up, Color.green);
                    else
                        Debug.DrawRay(traverseNode.Value, Vector3.up, Color.magenta);

                    traverseNode = traverseNode.Next;
                }

                Debug.DrawRay(Destination, Vector3.up, Color.red);
            }
        }
#endif
    }
}
