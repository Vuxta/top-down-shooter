using UnityEngine;
using System.Collections;

namespace VuxtaStudio.TDS.SteeringSystem
{
    public abstract class PathJob
    {
        public abstract void Execute();
    }
}
