using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SteeringAttribute
{
    public Vector3 SteeringForce = Vector3.zero;
    public List<Vector3> Path = new List<Vector3>();
    public Vector3 PreviousPos = Vector3.zero;
}
