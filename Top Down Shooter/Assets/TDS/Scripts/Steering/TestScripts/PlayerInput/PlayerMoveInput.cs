using UnityEngine;
using System.Collections;

namespace VuxtaStudio.TDS.SteeringSystem.Test
{
    public class PlayerMoveInput : MonoBehaviour
    {
        private SteeringController m_SteerController;

        private float m_ForwardVal = 0f;
        private float m_VerticalVal = 0f;

        private Vector3 m_PreviousMoveVelocity = Vector3.zero;
        private Vector3 m_MoveVelocity = Vector3.zero;

        private void Awake()
        {
            m_SteerController = GetComponent<SteeringController>();
        }

        // Update is called once per frame
        void Update()
        {
            // code that moves the player (in my case using a navmesh)
            if (Input.GetKey(KeyCode.Z) && Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;

                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100))
                {
                    //agent.destination = hit.point;
                    m_SteerController.CommandLayer.SetDestination(hit.point);
                }
            }


            if (Input.GetKey(KeyCode.W))
                m_ForwardVal = 5f;
            else if (Input.GetKey(KeyCode.S))
                m_ForwardVal = -5f;
            else
                m_ForwardVal = 0f;

            if (Input.GetKey(KeyCode.A))
                m_VerticalVal = -5f;
            else if (Input.GetKey(KeyCode.D))
                m_VerticalVal = 5f;
            else
                m_VerticalVal = 0f;

            m_MoveVelocity = Vector3.forward * m_ForwardVal + Vector3.right * m_VerticalVal;

            if (m_MoveVelocity != m_PreviousMoveVelocity)
            {
                m_SteerController.CommandLayer.MoveCmd(m_MoveVelocity);
                m_PreviousMoveVelocity = m_MoveVelocity;
            }
#if HTC_DEBUG
            steerBehaviour.SteerPath.DebugDraw();
#endif
        }
    }
}
