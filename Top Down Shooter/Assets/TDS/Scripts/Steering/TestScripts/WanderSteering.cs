using UnityEngine;
using System.Collections;
using UnityEngine.AI;

namespace VuxtaStudio.TDS.SteeringSystem.Test
{
    [RequireComponent(typeof(SteeringController))]
    public class WanderSteering : MonoBehaviour
    {
        private SteeringController m_SteerController;

        private float m_NextCommandTime = 0f;

        private void Awake()
        {
            m_SteerController = GetComponent<SteeringController>();
        }

        // Use this for initialization
        void Start()
        {
            m_NextCommandTime = Time.time + 5f;
        }

        // Update is called once per frame
        void Update()
        {
            // random select a point on the navmesh and move
            if (Time.time > m_NextCommandTime)
            {
                Random.InitState(this.gameObject.GetInstanceID() + (int)Time.time);

                Vector3 randomPos = Vector3.zero;
                if (GetRandomNavPoint(out randomPos))
                {
                    SetDestination(randomPos);
                }

                m_NextCommandTime = Time.time + Random.Range(1f, 20f);
            }

#if VUXTASTUDIO_DEBUG
            steerController.SteerPath.DebugDraw();
#endif
        }

        bool GetRandomNavPoint(out Vector3 randomPos)
        {
            float radius = 30f;
            randomPos = Vector3.zero;

            Vector3 randomDirection = Random.insideUnitSphere * radius;
            //randomDirection += transform.position;
            NavMeshHit hit;
            if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
            {
                randomPos = hit.position;
                return true;
            }
            else
                return false;

        }

        void SetDestination(Vector3 randomPos)
        {
            m_SteerController.CommandLayer.SetDestination(randomPos);
        }
    }
}
