﻿using System.IO;
using UnityEngine;

namespace VuxtaStudio.TDS.Global.Defines
{
    /// <summary>
    /// Defines for all file IO paths
    /// </summary>
    public static class FilePaths
    {
        public static string EnvironmentDirectory = Path.Combine(Application.dataPath, "env");
        public static string EnvironmentFile = Path.Combine(EnvironmentDirectory, "savedEnv.json");
    }
}