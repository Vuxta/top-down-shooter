﻿using System.Collections.Generic;
using UnityEngine;

namespace VuxtaStudio.TDS.AIControl
{
    public class SphereCastSensor : SensorBase
    {
        private float m_Angle = 120f;
        private float m_MaxDistance = 20f;

        private Collider[] m_CastObjects;
        private List<string> m_TargetTags = new List<string>();
        private LayerMask m_HitTestMask;

        public SphereCastSensor(Transform trans, LayerMask testMasks, float angle, float distance, params string[] targetTags) : base(trans)
        {
            m_HitTestMask = testMasks;
            m_Angle = angle;
            m_MaxDistance = distance;
            m_TargetTags.AddRange(targetTags);
        }

        public override bool TargetInRange(GameObject obj, float range)
        {
            return TargetInSight(obj.transform, range);
        }

        public override void SensorUpdate()
        {
            m_CastObjects = Physics.OverlapSphere(m_SensorTransform.position, m_MaxDistance);

            if (m_CastObjects == null || m_CastObjects.Length <= 0)
            {
                m_IsTriggered = false;
                m_TriggeredPos = Vector3.zero;
                m_TriggeredObject = null;
                return;
            }

            for (int i = 0; i < m_CastObjects.Length; i++)
            {
                if (!m_TargetTags.Contains(m_CastObjects[i].tag))
                {
#if UNITY_EDITOR && VUXTASTUDIO_DEBUG
                    Debug.DrawLine(m_SensorTransform.position, m_CastObjects[i].transform.position, Color.red);
#endif
                    continue;
                }

                if (Vector3.Angle(m_SensorTransform.forward, m_CastObjects[i].transform.position) > m_Angle)
                {
#if UNITY_EDITOR && VUXTASTUDIO_DEBUG
                    Debug.DrawLine(m_SensorTransform.position, m_CastObjects[i].transform.position, Color.yellow);
#endif
                    continue;
                }

                if (TargetInSight(m_CastObjects[i].transform, m_MaxDistance))
                {
#if UNITY_EDITOR && VUXTASTUDIO_DEBUG
                    Debug.DrawLine(m_SensorTransform.position, m_CastObjects[i].transform.position, Color.green);
#endif
                    m_IsTriggered = true;
                    m_TriggeredPos = m_CastObjects[i].transform.position;
                    m_TriggeredObject = m_CastObjects[i].gameObject;
                    return;
                }
                else
                {
#if UNITY_EDITOR && VUXTASTUDIO_DEBUG
                    Debug.DrawLine(m_SensorTransform.position, m_CastObjects[i].transform.position, Color.yellow);
#endif
                }

            }

            m_IsTriggered = false;
            m_TriggeredPos = Vector3.zero;
            m_TriggeredObject = null;
        }

        bool TargetInSight(Transform target, float distance)
        {
            Vector3 sightPosition = m_SensorTransform.position;
            Vector3 dir = target.position - sightPosition;

            if (Vector3.Angle(m_SensorTransform.forward, dir) > m_Angle)
            {
                return false;
            }

            RaycastHit hit = new RaycastHit();
            //Debug.DrawRay (headTransform.position, dir);
            Physics.Raycast(sightPosition, dir, out hit, distance, m_HitTestMask);

#if UNITY_EDITOR && VUXTASTUDIO_DEBUG
            Debug.DrawLine(sightPosition, target.position, Color.gray);
#endif

            return (hit.collider != null && target.gameObject == hit.collider.gameObject);
        }
    }
}
