﻿using System;
using UnityEngine;
using VuxtaStudio.Common.FiniteStateMachine;

namespace VuxtaStudio.TDS.AIControl
{
    public class IdlePatrolAndLookAroundState : IdleState
    {
        public StateMachine m_StateMachine;
        public IdleLookAroundState m_LookState;
        public IdlePatrolState m_PatrolState;

        public IdlePatrolAndLookAroundState(AIController controller, Action<Vector3, GameObject> onTargetInsight, Vector3[] patrolNodes, Vector3[] lookSequence) : base(controller, onTargetInsight)
        {
            m_LookState = new IdleLookAroundState(controller, onTargetInsight, OnLookSequenceComplete, lookSequence);
            m_PatrolState = new IdlePatrolState(controller, onTargetInsight, OnPatrolNodeReached, patrolNodes);
            m_StateMachine = new StateMachine();
        }

        public IdlePatrolAndLookAroundState(AIController controller, Action<Vector3, GameObject> onTargetInsight, Vector3[] patrolNodes) : base(controller, onTargetInsight)
        {
            m_LookState = new IdleLookAroundState(controller, onTargetInsight, OnLookSequenceComplete);
            m_PatrolState = new IdlePatrolState(controller, onTargetInsight, OnPatrolNodeReached, patrolNodes);
            m_StateMachine = new StateMachine();
        }

        public override void OnStateEnter()
        {
            m_StateMachine.ChangeState(m_PatrolState);
        }

        public override void OnStateExit()
        {
        }

        public override void OnStateUpdate()
        {
            m_StateMachine.OnMachineUpdate();
        }

        public void SetPatrolRoute(params Vector3[] patrolNodes)
        {
            m_PatrolState.SetPatrolRoute(patrolNodes);
        }

        private void OnPatrolNodeReached()
        {
            m_StateMachine.ChangeState(m_LookState);
        }

        private void OnLookSequenceComplete()
        {
            m_StateMachine.ChangeState(m_PatrolState);
        }
    }
}
