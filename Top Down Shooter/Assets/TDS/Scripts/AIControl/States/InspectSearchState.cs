﻿using System;
using UnityEngine;
using VuxtaStudio.Common.FiniteStateMachine;

namespace VuxtaStudio.TDS.AIControl
{
    public class InspectSearchState : InspectState
    {
        private Vector3 m_TargetedPosition = Vector3.zero;
        private GameObject m_TargetedObject;

        private const float m_InspectDuration = 3f;
        private float m_InspectTimer = 0f;

        private const float m_TargetLostDebounce = 0.3f;
        private float m_TargetLostTimer = 0f;

        private float m_FireRange = 20f;

        public InspectSearchState(AIController controller, Action onTargetLost, Action<Vector3, GameObject> onTargetInRange) : base(controller, onTargetLost, onTargetInRange)
        {
        }

        public override void OnStateEnter()
        {
            m_InspectTimer = 0f;
            m_TargetLostTimer = 0f;
            m_TargetedObject = (m_AIController.LockedObject != null) ? m_AIController.LockedObject : null;
            m_TargetedPosition = m_AIController.SuspectPosition;

            if (m_TargetedObject != null)
            {
                //Debug.LogFormat("target object : {0}", m_TargetedObject.name);
            }

            m_AIController.SteerController.CommandLayer.SetDestination(m_TargetedPosition);

            //Debug.LogFormat("target pos : {0}", m_TargetedPosition);
        }

        public override void OnStateExit()
        {
            m_AIController.SteerController.CommandLayer.Stop();
        }

        public override void OnStateUpdate()
        {
            if (m_TargetedObject != null && m_AIController.TargetInRange(m_TargetedObject, m_FireRange))
            {
                m_OnTargetInRange?.Invoke(m_TargetedObject.transform.position, m_TargetedObject);
                return;
            }

            if (!IsTargetReached())
                return;

            if (m_TargetedObject != null && m_TargetLostTimer < m_TargetLostDebounce)
            {
                m_TargetLostTimer += Time.fixedDeltaTime;
                return;
            }

            m_TargetLostTimer = 0f;

            if (!m_AIController.SensorTriggered(out m_TargetedPosition, out m_TargetedObject))
            {
                m_InspectTimer += Time.fixedDeltaTime;
            }
            else
            {
                m_InspectTimer = 0f;
                m_AIController.SteerController.CommandLayer.SetDestination(m_TargetedPosition);
            }

            if (m_InspectTimer > m_InspectDuration)
            {
                m_OnTargetLost?.Invoke();
                return;
            }
        }

        private bool IsTargetReached()
        {
            Vector3 targetWaypoint = m_AIController.SteerController.SteerPath.TargetWaypoint;
            float toTargetDistance = Vector3.Distance(targetWaypoint, m_AIController.SteerController.LocomotionComp.GetTransform().position);

            if (m_AIController.SteerController.SteerPath.IsFinished()
                && toTargetDistance < m_AIController.SteerController.SteerBehaviour.StoppingDistance)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
