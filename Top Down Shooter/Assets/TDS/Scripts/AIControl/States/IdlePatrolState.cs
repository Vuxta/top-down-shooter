﻿using System;
using UnityEngine;
using VuxtaStudio.Common.FiniteStateMachine;

namespace VuxtaStudio.TDS.AIControl
{
    public class IdlePatrolState : IdleState
    {
        private Vector3 m_TriggeredPos;
        private GameObject m_TriggeredObject;
        private Vector3[] m_PatrolNodes;
        private int m_PatrolNodeIndex = 0;
        private Vector3 m_CurrentDestination;
        private Action m_OnPatrolNodeReached;

        public IdlePatrolState(AIController controller, Action<Vector3, GameObject> onTargetInsight, Action onPatrolNodeReached, params Vector3[] patrolNodes) : base(controller, onTargetInsight)
        {
            m_OnPatrolNodeReached = onPatrolNodeReached;
            m_PatrolNodes = patrolNodes;
        }

        public override void OnStateEnter()
        {
            if (m_PatrolNodes != null)
                m_CurrentDestination = m_PatrolNodes[m_PatrolNodeIndex];
            else
                m_CurrentDestination = m_AIController.StartPosition;

            m_AIController.SteerController.CommandLayer.SetDestination(m_CurrentDestination);
        }

        public override void OnStateExit()
        {
            m_AIController.SteerController.CommandLayer.Stop();
        }

        public override void OnStateUpdate()
        {
            if (m_AIController.SensorTriggered(out m_TriggeredPos, out m_TriggeredObject))
            {
                m_OnTargetInsight?.Invoke(m_TriggeredPos, m_TriggeredObject);
                return;
            }

            Vector3 targetWaypoint = m_AIController.SteerController.SteerPath.TargetWaypoint;
            float toTargetDistance = Vector3.Distance(targetWaypoint, m_AIController.SteerController.LocomotionComp.GetTransform().position);

            if (m_AIController.SteerController.SteerPath.IsFinished()
                && toTargetDistance < m_AIController.SteerController.SteerBehaviour.StoppingDistance)
            {
                m_CurrentDestination = GetNextNode();
                //TODO: do callback for distination reached
                if (m_OnPatrolNodeReached != null)
                {
                    m_OnPatrolNodeReached.Invoke();
                }
                else
                {
                    m_AIController.SteerController.CommandLayer.SetDestination(m_CurrentDestination);
                }
            }
        }

        public void SetPatrolRoute(params Vector3[] patrolNodes)
        {
            m_PatrolNodes = patrolNodes;
        }

        private Vector3 GetNextNode()
        {
            if (m_PatrolNodes == null)
            {
                return m_CurrentDestination;
            }

            ++m_PatrolNodeIndex;

            if (m_PatrolNodeIndex >= m_PatrolNodes.Length)
            {
                m_PatrolNodeIndex = 0;
            }

            return m_PatrolNodes[m_PatrolNodeIndex];
        }
    }
}
