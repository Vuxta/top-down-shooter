﻿using System;
using UnityEngine;
using VuxtaStudio.Common.FiniteStateMachine;

namespace VuxtaStudio.TDS.AIControl
{
    public class DummyAttackState : AttackState
    {
        private GameObject m_TargetedObject;
        private Vector3 m_LastSeenPosition;

        private const float m_TargetLostDuration = 1f;
        private float m_TargetLostTimer = 0f;
        private float m_FireRange = 20f;

        public DummyAttackState(AIController controller, Action<Vector3, GameObject> onTargetOutOfRange) : base(controller, onTargetOutOfRange)
        {
        }

        public override void OnStateEnter()
        {
            m_TargetLostTimer = 0f;
            m_TargetedObject = m_AIController.LockedObject;
            m_LastSeenPosition = m_AIController.SuspectPosition;
        }

        public override void OnStateExit()
        {
        }

        public override void OnStateUpdate()
        {
            if (m_AIController.TargetInRange(m_TargetedObject, m_FireRange))
            {
                m_LastSeenPosition = m_TargetedObject.transform.position;
                m_AIController.FireAt(m_LastSeenPosition);
                return;
            }

            m_TargetLostTimer += Time.fixedDeltaTime;

            if (m_TargetLostTimer > m_TargetLostDuration)
            {
                m_OnTargetOutOfRange?.Invoke(m_LastSeenPosition, m_TargetedObject);
                return;
            }

        }
    }
}
