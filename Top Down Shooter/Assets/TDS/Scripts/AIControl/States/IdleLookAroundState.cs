﻿using System;
using UnityEngine;
using VuxtaStudio.Common.FiniteStateMachine;

namespace VuxtaStudio.TDS.AIControl
{
    public class IdleLookAroundState : IdleState
    {
        private Vector3 m_TriggeredPos;
        private GameObject m_TriggeredObject;

        private Vector3[] m_LookSequence = new Vector3[] { Vector3.forward, -1f * Vector3.forward, -1f * Vector3.right, Vector3.right };
        private Vector3 m_CurrentLookDir;
        private const float m_LookDuration = 1f;
        private float m_LookTimer = 0f;
        private int m_LookSequenceIndex = 0;

        private Action m_OnLookSequenceComplete;
        private const float m_LookSequenceDuration = 5f;
        private float m_LookSequenceTimer = 0f;

        public IdleLookAroundState(AIController controller, Action<Vector3, GameObject> onTargetInsight, Action onLookSequenceComplete, params Vector3[] lookSequence) : base(controller, onTargetInsight)
        {
            m_LookSequence = lookSequence;
            m_OnLookSequenceComplete = onLookSequenceComplete;
        }

        public IdleLookAroundState(AIController controller, Action<Vector3, GameObject> onTargetInsight, Action onLookSequenceComplete) : base(controller, onTargetInsight)
        {
            m_OnLookSequenceComplete = onLookSequenceComplete;
        }

        public override void OnStateEnter()
        {
            m_LookTimer = 0f;
            m_LookSequenceTimer = 0f;
            m_CurrentLookDir = m_AIController.SteerController.LocomotionComp.GetTransform().forward;
        }

        public override void OnStateExit()
        {
        }

        public override void OnStateUpdate()
        {
            if (m_AIController.SensorTriggered(out m_TriggeredPos, out m_TriggeredObject))
            {
                m_OnTargetInsight?.Invoke(m_TriggeredPos, m_TriggeredObject);
                return;
            }

            m_LookTimer += Time.fixedDeltaTime;
            m_LookSequenceTimer += Time.fixedDeltaTime;

            if (m_LookTimer > m_LookDuration)
            {
                m_LookTimer = 0;
                m_CurrentLookDir = GetNextLookSequence();
            }

            if (m_LookSequenceTimer > m_LookSequenceDuration)
            {
                m_OnLookSequenceComplete?.Invoke();
            }

            m_AIController.SteerController.LocomotionComp.TurnToDirection(m_CurrentLookDir);
        }

        private Vector3 GetNextLookSequence()
        {
            ++m_LookSequenceIndex;

            if (m_LookSequenceIndex >= m_LookSequence.Length)
            {
                m_LookSequenceIndex = 0;
            }

            return m_LookSequence[m_LookSequenceIndex];
        }
    }
}
