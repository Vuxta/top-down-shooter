﻿using System;
using UnityEngine;
using VuxtaStudio.Common.FiniteStateMachine;

namespace VuxtaStudio.TDS.AIControl
{
    public class DummyIdleState : IdleState
    {
        private Vector3 m_TriggeredPos;
        private GameObject m_TriggeredObject;

        public DummyIdleState(AIController controller, Action<Vector3, GameObject> action) : base(controller, action)
        {
        }

        public override void OnStateEnter()
        {
        }

        public override void OnStateExit()
        {
        }

        public override void OnStateUpdate()
        {
            if (m_AIController.SensorTriggered(out m_TriggeredPos, out m_TriggeredObject))
            {
                m_OnTargetInsight?.Invoke(m_TriggeredPos, m_TriggeredObject);
            }
        }
    }
}
