﻿using System;
using UnityEngine;
using VuxtaStudio.Common.FiniteStateMachine;

namespace VuxtaStudio.TDS.AIControl
{
    public abstract class IdleState : AIControllerStateBase
    {
        protected Action<Vector3, GameObject> m_OnTargetInsight;

        public IdleState(AIController controller, Action<Vector3, GameObject> onTargetInsight) : base(controller)
        {
            m_OnTargetInsight = onTargetInsight;
        }
    }
}
