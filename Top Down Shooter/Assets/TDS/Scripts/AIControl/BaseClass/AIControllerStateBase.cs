﻿using VuxtaStudio.Common.FiniteStateMachine;

namespace VuxtaStudio.TDS.AIControl
{
    public abstract class AIControllerStateBase : IState
    {
        protected AIController m_AIController;

        public AIControllerStateBase(AIController controller)
        {
            m_AIController = controller;
        }

        public abstract void OnStateEnter();
        public abstract void OnStateExit();
        public abstract void OnStateUpdate();
    }
}
