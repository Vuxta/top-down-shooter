﻿using UnityEngine;
using VuxtaStudio.Common.FiniteStateMachine;
using VuxtaStudio.TDS.SteeringSystem;

namespace VuxtaStudio.TDS.AIControl
{
    public abstract class AIController : MonoBehaviour
    {
        public SteeringController SteerController { get; private set; }
        public Vector3 StartPosition { get; private set; }
        public Vector3 CurrentPosition { get; private set; }
        public Vector3 SuspectPosition { get; private set; }
        public GameObject LockedObject { get; private set; }

        [SerializeField]
        protected LayerMask m_SensorLayers = ~(0);

        [SerializeField]
        protected string[] m_TargetTags = new string[] { "Player" };

        protected SensorBase[] m_Sensors;

        protected StateMachine m_StateMachine;
        protected IdleState m_Idle;
        protected InspectState m_Inspect;
        protected AttackState m_Attack;

        protected abstract void StatesInit();
        protected abstract void SensorsInit();

        public abstract void FireAt(Vector3 pos);

        private void Awake()
        {
            SensorsInit();
            SteerController = GetComponent<SteeringController>();

            m_StateMachine = new StateMachine();
            StatesInit();
        }

        private void Start()
        {
            if (SteerController != null && SteerController.LocomotionComp != null)
            {
                StartPosition = SteerController.LocomotionComp.GetTransform().position;
            }
            else
            {
                StartPosition = this.transform.position;
            }

            m_StateMachine.ChangeState(m_Idle);
        }

        private void FixedUpdate()
        {
            if (SteerController != null && SteerController.LocomotionComp != null)
            {
                CurrentPosition = SteerController.LocomotionComp.GetTransform().position;
            }
            else
            {
                CurrentPosition = this.transform.position;
            }

            UpdateSensors();
            m_StateMachine.OnMachineUpdate();
        }

        private void UpdateSensors()
        {
            for (int i = 0; i < m_Sensors.Length; i++)
            {
                m_Sensors[i].SensorUpdate();
            }
        }

        protected virtual void OnIdleTargetInsight(Vector3 pos, GameObject obj)
        {
            SuspectPosition = pos;
            LockedObject = obj;
            m_StateMachine.ChangeState(m_Inspect);
        }

        protected virtual void OnInspectTargetLost()
        {
            m_StateMachine.ChangeState(m_Idle);
        }

        protected virtual void OnInspectTargetInRange(Vector3 lastSeenPosition, GameObject obj)
        {
            SuspectPosition = lastSeenPosition;
            LockedObject = obj;
            m_StateMachine.ChangeState(m_Attack);
        }

        protected virtual void OnAttackTargetOutOfRange(Vector3 lastSeenPosition, GameObject obj)
        {
            SuspectPosition = lastSeenPosition;
            LockedObject = obj;
            m_StateMachine.ChangeState(m_Inspect);
        }

        public bool SensorTriggered(out Vector3 pos, out GameObject obj)
        {
            pos = Vector3.zero;
            obj = null;

            for (int i = 0; i < m_Sensors.Length; i++)
            {
                if (m_Sensors[i].SensorTriggered(out pos, out obj))
                {
                    return true;
                }

            }

            return false;
        }

        public bool TargetInRange(GameObject obj, float range)
        {
            for (int i = 0; i < m_Sensors.Length; i++)
            {
                if (m_Sensors[i].TargetInRange(obj, range))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
