﻿using System;
using UnityEngine;
using VuxtaStudio.Common.FiniteStateMachine;

namespace VuxtaStudio.TDS.AIControl
{
    public abstract class InspectState : AIControllerStateBase
    {
        protected Action m_OnTargetLost;
        protected Action<Vector3, GameObject> m_OnTargetInRange;

        public InspectState(AIController controller, Action onTargetLost, Action<Vector3, GameObject> onTargetInRange) : base(controller)
        {
            m_OnTargetLost = onTargetLost;
            m_OnTargetInRange = onTargetInRange;
        }
    }
}
