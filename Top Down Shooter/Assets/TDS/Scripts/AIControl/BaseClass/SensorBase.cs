﻿using UnityEngine;

namespace VuxtaStudio.TDS.AIControl
{
    public abstract class SensorBase
    {
        protected Transform m_SensorTransform;
        protected bool m_IsTriggered = false;
        protected Vector3 m_TriggeredPos = Vector3.zero;
        protected GameObject m_TriggeredObject;

        public SensorBase(Transform trans)
        {
            m_SensorTransform = trans;
        }

        public abstract void SensorUpdate();
        public abstract bool TargetInRange(GameObject obj, float distance);

        public bool SensorTriggered(out Vector3 triggeredPos, out GameObject triggeredObject)
        {
            if (m_IsTriggered)
            {
                triggeredPos = m_TriggeredPos;
                triggeredObject = m_TriggeredObject;
                return true;
            }
            else
            {
                triggeredPos = Vector3.zero;
                triggeredObject = null;
                return false;
            }
        }
    }
}
