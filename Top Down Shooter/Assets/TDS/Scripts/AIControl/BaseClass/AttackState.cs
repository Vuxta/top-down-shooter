﻿using System;
using UnityEngine;
using VuxtaStudio.Common.FiniteStateMachine;

namespace VuxtaStudio.TDS.AIControl
{
    public abstract class AttackState : AIControllerStateBase
    {
        protected Action<Vector3, GameObject> m_OnTargetOutOfRange;

        public AttackState(AIController controller, Action<Vector3, GameObject> onTargetOutOfRange) : base(controller)
        {
            m_OnTargetOutOfRange = onTargetOutOfRange;
        }
    }
}
