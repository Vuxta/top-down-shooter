﻿using UnityEngine;
using System.Collections.Generic;


namespace VuxtaStudio.TDS.PhysicsSystem
{
    /// <summary>
    /// Class for layer masks
    /// </summary>
    public static class PhysicsCollisionMatrixLayerMasks
    {
        private static Dictionary<int, int> m_MasksByLayer;

        /// <summary>
        /// Init filters of each corresponding layer
        /// </summary>
        private static void Init()
        {
            if (m_MasksByLayer.Count > 0)
                return;

            m_MasksByLayer = new Dictionary<int, int>();
            for (int i = 0; i < 32; i++)
            {
                int mask = 0;
                for (int j = 0; j < 32; j++)
                {
                    if (!Physics.GetIgnoreLayerCollision(i, j))
                    {
                        mask |= 1 << j;
                    }
                }
                m_MasksByLayer.Add(i, mask);
            }
        }

        /// <summary>
        /// Get corresponding layers by specific layer
        /// </summary>
        /// <param name="layer"></param>
        /// <returns></returns>
        public static int MaskForLayer(int layer)
        {
            Init();

            return m_MasksByLayer[layer];
        }
    }
}