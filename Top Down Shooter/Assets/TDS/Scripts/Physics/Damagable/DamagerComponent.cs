﻿// <copyright file="DamagerComponent.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>01/08/2019 16:57:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, Janunary 08, 2019

using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEditor;

namespace VuxtaStudio.TDS.PhysicsSystem
{
    /// <summary>
    /// Unity event send when damager hits
    /// </summary>
    [Serializable]
    public class DamagerHitEvent : UnityEvent<List<DamagableComponent>, Collider> { }

    /// <summary>
    /// Component which performs damage to damagable components
    /// </summary>
    public class DamagerComponent : MonoBehaviour
    {
        #region Serialized Private Fields

        [SerializeField]
        private DamageInfo m_DamageInfo;

        [SerializeField]
        private bool m_Explodable = false;

        [SerializeField]
        private float m_ExplodeRadius = 3f;

        [SerializeField]
        private DamagerHitEvent m_OnDamagerHit;

        #endregion

        #region Private Fields

        private List<DamagableComponent> m_DamagableComponentList = new List<DamagableComponent>();
        private Collider[] m_ContactResult = new Collider[16];

        #endregion

        #region Public Fields

        public DamagerHitEvent OnDamagerHit
        {
            get
            {
                return m_OnDamagerHit;
            }
        }

        #endregion

        #region Unity MonoBehaviour Methods

        private void OnTriggerEnter(Collider collision)
        {
            m_DamagableComponentList.Clear();
            if (m_Explodable)
            {
                int hitCount = Physics.OverlapSphereNonAlloc(this.transform.position, m_ExplodeRadius, m_ContactResult, PhysicsCollisionMatrixLayerMasks.MaskForLayer(gameObject.layer));

                for (int i = 0; i < hitCount; i++)
                {
                    DamagableComponent damagableComponent = m_ContactResult[i].GetComponent<DamagableComponent>();

                    if (damagableComponent != null)
                        m_DamagableComponentList.Add(damagableComponent);
                }
            }
            else
            {
                DamagableComponent damagableComponent = collision.gameObject.GetComponent<DamagableComponent>();

                if (damagableComponent != null)
                    m_DamagableComponentList.Add(damagableComponent);
            }

            for (int i = 0; i < m_DamagableComponentList.Count; i++)
            {
                m_DamagableComponentList[i].TakeDamage(m_DamageInfo);
            }

            m_OnDamagerHit.Invoke(m_DamagableComponentList, collision);
        }

        /// <summary>
        /// Debug for explotion area if explodable is set
        /// </summary>
        private void OnDrawGizmos()
        {
#if UNITY_EDITOR
            if (m_Explodable)
            {
                Handles.color = Color.yellow;
                Handles.DrawWireArc(this.transform.position, Vector3.forward, Vector3.up, 360f, m_ExplodeRadius);
            }
#endif
        }

        #endregion
    }
}
