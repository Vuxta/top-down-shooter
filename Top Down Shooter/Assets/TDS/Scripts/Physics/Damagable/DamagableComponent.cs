﻿// <copyright file="DamagableComponent.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>01/08/2019 16:57:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, Janunary 08, 2019

using UnityEngine;
using UnityEngine.Events;
using System;

namespace VuxtaStudio.TDS.PhysicsSystem
{
    /// <summary>
    /// Information when take damage
    /// </summary>
    [Serializable]
    public struct DamageInfo
    {
        public int value;
    }

    /// <summary>
    /// Information when gain health
    /// </summary>
    [Serializable]
    public struct GainHealthInfo
    {
        public int value;
    }

    /// <summary>
    /// Event when taking damage
    /// </summary>
    [Serializable]
    public class TakeDamageEvent : UnityEvent<DamageInfo, int> { }

    /// <summary>
    /// Event when taking damage
    /// </summary>
    [Serializable]
    public class GainHealthEvent : UnityEvent<GainHealthInfo, int> { }

    /// <summary>
    /// Event when taking damage
    /// </summary>
    [Serializable]
    public class DeadEvent : UnityEvent<DamageInfo> { }

    /// <summary>
    /// Component that manages health points of object
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class DamagableComponent : MonoBehaviour
    {
        #region Private Serlialized Fields

        [SerializeField]
        private int m_MaxHealthPoint = 10;

        [SerializeField]
        private TakeDamageEvent m_OnTakeDamage;

        [SerializeField]
        private GainHealthEvent m_OnGainHealth;

        [SerializeField]
        private DeadEvent m_OnDie;

        #endregion

        #region Private Fields

        private int m_CurrentHealth = 0;
        private bool m_Dead = false;

        #endregion

        #region Public Fields

        public TakeDamageEvent OnTakeDamageEvent
        {
            get
            {
                return m_OnTakeDamage;
            }
        }

        public GainHealthEvent OnGainHealth
        {
            get
            {
                return m_OnGainHealth;
            }
        }

        public DeadEvent OnDie
        {
            get
            {
                return m_OnDie;
            }
        }

        #endregion

        #region Unity MonoBehaviour Methods

        /// <summary>
        /// Init current health as maximum health
        /// </summary>
        private void Awake()
        {
            m_CurrentHealth = m_MaxHealthPoint;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Called when taking damages, update health points and calling event of dead/take damage
        /// </summary>
        /// <param name="info">Damage information</param>
        public void TakeDamage(DamageInfo info)
        {
            if (m_Dead)
                return;

            m_CurrentHealth -= info.value;

            if (m_CurrentHealth > 0)
            {
                m_OnTakeDamage.Invoke(info, m_CurrentHealth);
            }
            else
            {
                m_CurrentHealth = 0;
                m_OnDie.Invoke(info);
            }
        }

        /// <summary>
        /// Called when gain health, update health points, and calling gain health event
        /// </summary>
        /// <param name="info">Gain health info</param>
        public void GainHealth(GainHealthInfo info)
        {
            if (m_Dead)
                return;

            m_CurrentHealth += info.value;
            if (m_CurrentHealth > m_MaxHealthPoint)
            {
                m_CurrentHealth = m_MaxHealthPoint;
            }

            m_OnGainHealth.Invoke(info, m_CurrentHealth);
        }

        #endregion
    }
}
