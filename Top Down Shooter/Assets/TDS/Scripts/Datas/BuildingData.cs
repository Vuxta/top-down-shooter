﻿using System;
using UnityEngine;
using VuxtaStudio.Common;

namespace VuxtaStudio.TDS.Datas
{
    [Serializable]
    public class BuildingData : JsonData
    {
        public Vector3 Position;
        public int BuildingId;
    }
}
