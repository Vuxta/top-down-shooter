﻿using UnityEngine;
using VuxtaStudio.Common;
using System.Collections.Generic;
using System;

namespace VuxtaStudio.TDS.Datas
{
    [Serializable]
    public class SurroundingData : JsonData
    {
        public bool IsSeeded = false;
        public float Seed;
        public float Length;
        public float Width;
        public Vector3 CenterPosition = Vector3.zero;
        public bool EdgesAreWalls = false;
        public int FillPercent = 45;
        public int SmoothAmount = 40;
        public List<Vector3> PositionList;
    }
}
