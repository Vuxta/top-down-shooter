﻿using UnityEngine;
using VuxtaStudio.Common;
using System.Collections.Generic;
using System;

namespace VuxtaStudio.TDS.Datas
{
    [Serializable]
    public class EnvironmentData : JsonData
    {
        public SurroundingData Surroundings;
        public List<BuildingData> Buildings;
    }
}
