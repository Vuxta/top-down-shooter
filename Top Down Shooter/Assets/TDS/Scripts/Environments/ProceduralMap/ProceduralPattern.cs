﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace VuxtaStudio.TDS.Environment
{
    public class ProceduralPattern
    {
        public static int[,] GenerateCellularAutomataVNSmoothed(int width, int height, float seed, int fillPercent, bool edgesAreWalls, int smoothAmount)
        {
            int[,] map = new int[width, height];

            map = GenerateCellularAutomata(width, height, seed, fillPercent, edgesAreWalls);

            map = SmoothVNCellularAutomata(map, edgesAreWalls, smoothAmount);

            map = RemoveUnconnectedCarven(map);

            return map;
        }

        public static int[,] GenerateCellularAutomataMooreSmoothed(int width, int height, float seed, int fillPercent, bool edgesAreWalls, int smoothAmount)
        {
            int[,] map = new int[width, height];

            map = GenerateCellularAutomata(width, height, seed, fillPercent, edgesAreWalls);

            map = SmoothMooreCellularAutomata(map, edgesAreWalls, smoothAmount);

            map = RemoveUnconnectedCarven(map);

            return map;
        }

        /// <summary>
        /// Creates the basis for our Advanced Cellular Automata functions.
        /// We can then input this map into different functions depending on
        /// what type of neighbourhood we want
        /// </summary>
        /// <param name="map">The array to be modified</param>
        /// <param name="seed">The seed we will use</param>
        /// <param name="fillPercent">The amount we want the map filled</param>
        /// <param name="edgesAreWalls">Whether we want the edges to be walls</param>
        /// <returns>The modified map array</returns>
        public static int[,] GenerateCellularAutomata(int width, int height, float seed, int fillPercent, bool edgesAreWalls)
        {
            //Seed our random number generator
            System.Random rand = new System.Random(seed.GetHashCode());

            //Set up the size of our array
            int[,] map = new int[width, height];

            //Start looping through setting the cells.
            for (int x = 0; x < map.GetUpperBound(0); x++)
            {
                for (int y = 0; y < map.GetUpperBound(1); y++)
                {
                    if (edgesAreWalls && (x == 0 || x == map.GetUpperBound(0) - 1 || y == 0 || y == map.GetUpperBound(1) - 1))
                    {
                        //Set the cell to be active if edges are walls
                        map[x, y] = 1;
                    }
                    else
                    {
                        //Set the cell to be active if the result of rand.Next() is less than the fill percentage
                        map[x, y] = (rand.Next(0, 100) < fillPercent) ? 1 : 0;
                    }
                }
            }
            return map;
        }

        /// <summary>
        /// Smooths the map using the von Neumann Neighbourhood rules
        /// </summary>
        /// <param name="map">The map we will Smooth</param>
        /// <param name="edgesAreWalls">Whether the edges are walls or not</param>
        /// <param name="smoothCount">The amount we will loop through to smooth the array</param>
        /// <returns>The modified map array</returns>
        public static int[,] SmoothVNCellularAutomata(int[,] map, bool edgesAreWalls, int smoothCount)
        {
            for (int i = 0; i < smoothCount; i++)
            {
                for (int x = 0; x < map.GetUpperBound(0); x++)
                {
                    for (int y = 0; y < map.GetUpperBound(1); y++)
                    {
                        //Get the surrounding tiles
                        int surroundingTiles = GetVNSurroundingTiles(map, x, y, edgesAreWalls);

                        if (edgesAreWalls && (x == 0 || x == map.GetUpperBound(0) - 1 || y == 0 || y == map.GetUpperBound(1)))
                        {
                            map[x, y] = 1; //Keep our edges as walls
                        }
                        //von Neuemann Neighbourhood requires only 3 or more surrounding tiles to be changed to a tile
                        else if (surroundingTiles > 2)
                        {
                            map[x, y] = 1;
                        }
                        //If we have less than 2 neighbours, set the tile to be inactive
                        else if (surroundingTiles < 2)
                        {
                            map[x, y] = 0;
                        }
                        //Do nothing if we have 2 neighbours
                    }
                }
            }
            return map;
        }

        /// <summary>
        /// Gets the surrounding tiles using the von Neumann Neighbourhood rules. This neighbourhood only checks the direct neighbours, i.e. Up, Left, Down Right
        /// </summary>
        /// <param name="map">The map we are checking</param>
        /// <param name="x">The x position we are checking</param>
        /// <param name="y">The y position we are checking</param>
        /// <returns>The amount of neighbours the tile map[x,y] has</returns>
        static int GetVNSurroundingTiles(int[,] map, int x, int y, bool edgesAreWalls)
        {
            /* von Neumann Neighbourhood looks like this ('T' is our Tile, 'N' is our Neighbour)
            *
            *   N
            * N T N
            *   N
            *
            */

            int tileCount = 0;

            //If we are not touching the left side of the map
            if (x - 1 > 0)
            {
                tileCount += map[x - 1, y];
            }
            else if (edgesAreWalls)
            {
                tileCount++;
            }

            //If we are not touching the bottom of the map
            if (y - 1 > 0)
            {
                tileCount += map[x, y - 1];
            }
            else if (edgesAreWalls)
            {
                tileCount++;
            }

            //If we are not touching the right side of the map
            if (x + 1 < map.GetUpperBound(0))
            {
                tileCount += map[x + 1, y];
            }
            else if (edgesAreWalls)
            {
                tileCount++;
            }

            //If we are not touching the top of the map
            if (y + 1 < map.GetUpperBound(1))
            {
                tileCount += map[x, y + 1];
            }
            else if (edgesAreWalls)
            {
                tileCount++;
            }

            return tileCount;
        }

        /// <summary>
        /// Smoothes a map using Moore's Neighbourhood Rules. Moores Neighbourhood consists of all neighbours of the tile, including diagonal neighbours
        /// </summary>
        /// <param name="map">The map to modify</param>
        /// <param name="edgesAreWalls">Whether our edges should be walls</param>
        /// <param name="smoothCount">The amount we will loop through to smooth the array</param>
        /// <returns>The modified map</returns>
        public static int[,] SmoothMooreCellularAutomata(int[,] map, bool edgesAreWalls, int smoothCount)
        {
            for (int i = 0; i < smoothCount; i++)
            {
                for (int x = 0; x < map.GetUpperBound(0); x++)
                {
                    for (int y = 0; y < map.GetUpperBound(1); y++)
                    {
                        int surroundingTiles = GetMooreSurroundingTiles(map, x, y, edgesAreWalls);

                        //Set the edge to be a wall if we have edgesAreWalls to be true
                        if (edgesAreWalls && (x == 0 || x == (map.GetUpperBound(0) - 1) || y == 0 || y == (map.GetUpperBound(1) - 1)))
                        {
                            map[x, y] = 1;
                        }
                        //If we have more than 4 neighbours, change to an active cell
                        else if (surroundingTiles > 4)
                        {
                            map[x, y] = 1;
                        }
                        //If we have less than 4 neighbours, change to be an inactive cell
                        else if (surroundingTiles < 4)
                        {
                            map[x, y] = 0;
                        }

                        //If we have exactly 4 neighbours, do nothing
                    }
                }
            }
            return map;
        }


        /// <summary>
        /// Gets the surrounding amount of tiles using the Moore Neighbourhood
        /// </summary>
        /// <param name="map">The map to check</param>
        /// <param name="x">The x position we are checking</param>
        /// <param name="y">The y position we are checking</param>
        /// <param name="edgesAreWalls">Whether the edges are walls</param>
        /// <returns>An int with the amount of surrounding tiles</returns>
        public static int GetMooreSurroundingTiles(int[,] map, int x, int y, bool edgesAreWalls)
        {
            /* Moore Neighbourhood looks like this ('T' is our tile, 'N' is our neighbours)
             *
             * N N N
             * N T N
             * N N N
             *
             */

            int tileCount = 0;

            //Cycle through the x values
            for (int neighbourX = x - 1; neighbourX <= x + 1; neighbourX++)
            {
                //Cycle through the y values
                for (int neighbourY = y - 1; neighbourY <= y + 1; neighbourY++)
                {
                    if (neighbourX >= 0 && neighbourX < map.GetUpperBound(0) && neighbourY >= 0 && neighbourY < map.GetUpperBound(1))
                    {
                        //We don't want to count the tile we are checking the surroundings of
                        if (neighbourX != x || neighbourY != y)
                        {
                            tileCount += map[neighbourX, neighbourY];
                        }
                    }
                }
            }
            return tileCount;
        }

        public static int[,] RemoveUnconnectedCarven(int[,] map)
        {
            //Identify Carvens, start from index 2
            int fillIndex = 2;
            List<List<Vector2Int>> carvenCounts = new List<List<Vector2Int>>();

            //TODO: use dictionary for recording down the coordinates while identify carven,
            //so we dont need to use brute force to walk through entire map again

            for (int x = 0; x < map.GetUpperBound(0); x++)
            {
                for (int y = 0; y < map.GetUpperBound(1); y++)
                {
                    if (map[x, y] != 0)
                        continue;

                    List<Vector2Int> coordinates = new List<Vector2Int>();
                    FloodFillCavern(ref map, ref coordinates, x, y, fillIndex);
                    carvenCounts.Add(coordinates);
                    ++fillIndex;
                }
            }

            //Find most elements index
            int maxIndex = -1;
            int maxCount = -1;

            for (int i = 0; i < carvenCounts.Count; i++)
            {
                if (carvenCounts[i].Count > maxCount)
                {
                    maxCount = carvenCounts[i].Count;
                    maxIndex = i;
                }
            }

            //Turn smaller carvens in to walls
            int fillElement = 0;
            for (int i = 0; i < carvenCounts.Count; i++)
            {
                if (i == maxIndex)
                {
                    fillElement = 0;
                }
                else
                {
                    fillElement = 1;
                }

                for (int j = 0; j < carvenCounts[i].Count; j++)
                {
                    map[carvenCounts[i][j].x, carvenCounts[i][j].y] = fillElement;
                }
            }

            return map;
        }

        static void FloodFillCavern(ref int[,] map, ref List<Vector2Int> carvens, int x, int y, int fillId)
        {
            if ((x < 0) || (x >= map.GetUpperBound(0)))
                return;

            if ((y < 0) || (y >= map.GetUpperBound(1)))
                return;

            if (map[x, y] != 0)
                return;

            map[x, y] = fillId;
            carvens.Add(new Vector2Int(x, y));

            FloodFillCavern(ref map, ref carvens, x + 1, y, fillId);
            FloodFillCavern(ref map, ref carvens, x, y + 1, fillId);
            FloodFillCavern(ref map, ref carvens, x - 1, y, fillId);
            FloodFillCavern(ref map, ref carvens, x, y - 1, fillId);
        }
    }
}
