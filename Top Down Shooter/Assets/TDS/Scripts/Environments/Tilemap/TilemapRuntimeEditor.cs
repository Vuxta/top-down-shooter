﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using VuxtaStudio.TDS.Datas;

namespace VuxtaStudio.TDS.Environment
{
    public class TilemapRuntimeEditor : FreeTilemapGenerator
    {
        [SerializeField]
        private Vector3 m_CenterPosition = Vector3.zero;

        [Tooltip("Distance in X axis")]
        [SerializeField]
        private float m_Length = 10f;

        [Tooltip("Distance in Z axis")]
        [SerializeField]
        private float m_Width = 10f;

        [SerializeField]
        private bool m_SnapToGrid = true;

        [SerializeField]
        private GameObject m_Floor;

        [SerializeField]
        private bool m_RandomSeed = false;

        [SerializeField]
        private float m_Seed = 1.215f;

        [SerializeField]
        private bool m_EdgesAreWalls = false;

        [SerializeField]
        private int m_FillPercent = 45;

        [SerializeField]
        private int m_SmoothAmount = 40;

        private float m_MinX;
        private float m_MaxX;
        private float m_MinZ;
        private float m_MaxZ;

        private BuildingObject m_CurrentBuilding;
        private GameObject m_CurrentTile;
        private Vector3 m_BuildingCenterOffset = Vector3.zero;
        private Vector3 m_BuildingExtents = Vector3.one * 0.5f;

        private void Start()
        {
            m_MinX = m_CenterPosition.x - m_Length / 2;
            m_MaxX = m_CenterPosition.x + m_Length / 2;
            m_MinZ = m_CenterPosition.z - m_Width / 2;
            m_MaxZ = m_CenterPosition.z + m_Width / 2;
        }

        private void Update()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;

            if (m_CurrentBuilding != null)
            {
                if (!Physics.Raycast(ray, out hitInfo, Mathf.Infinity, 1 << m_Floor.gameObject.layer))
                {
                    return;
                }

                Vector3 currentPosition = hitInfo.point + m_BuildingCenterOffset;

                if (m_SnapToGrid)
                {
                    currentPosition = SnapToGrid(currentPosition);
                }

                if (currentPosition.x < m_MinX
                    || currentPosition.x > m_MaxX
                    || currentPosition.z < m_MinZ
                    || currentPosition.z > m_MaxZ)
                    return;

                m_CurrentTile.transform.position = currentPosition;

                if (HasObject(currentPosition, m_CurrentBuilding))
                {
                    m_CurrentTile.GetComponent<MeshRenderer>().material.color = Color.red;
                    return;
                }

                m_CurrentTile.GetComponent<MeshRenderer>().material.color = Color.white;

                if (Input.GetMouseButtonUp(0))
                {
                    PlaceBuilding(currentPosition);
                }
            }
            else
            {
                if (!Physics.Raycast(ray, out hitInfo, Mathf.Infinity))
                {
                    return;
                }

                if (Input.GetMouseButtonUp(1))
                {
                    RemoveObject(hitInfo.transform.gameObject);
                }
            }
        }

        public void GenerateSurroundings()
        {
            float seed = (m_RandomSeed) ? Time.time : m_Seed;
            SurroundingData data = new SurroundingData();

            Debug.LogFormat("Generate surroundings by seed: {0}", seed);

            data.IsSeeded = true;
            data.Length = m_Length;
            data.Width = m_Width;
            data.Seed = seed;
            data.FillPercent = m_FillPercent;
            data.EdgesAreWalls = m_EdgesAreWalls;
            data.SmoothAmount = m_SmoothAmount;

            ReloadSurroundings(data);
        }

        public void Save()
        {
            SaveEnvironmentData();
        }

        public void Load()
        {
            LoadEnvironmentData();
        }

        public void PlaceBuilding(Vector3 pos)
        {
            if (PlaceObject(pos, m_CurrentBuilding, m_SnapToGrid))
            {
                m_CurrentTile.SetActive(false);
                m_CurrentBuilding = null;
            }
        }

        public void SpawnBuilding(int id)
        {
            m_CurrentBuilding =
                m_BuildingListProfile.BuildingList.Find(x => x.BuildingId == id);

            if (m_CurrentBuilding == null)
            {
                Debug.LogWarningFormat("Building ID {0}: does not exist in the profile, skipping...");
                return;
            }

            m_BuildingCenterOffset = Vector3.zero;
            m_BuildingExtents = m_CurrentBuilding.HalfExtents;

            CreateTile(m_BuildingExtents);
        }

        private void CreateTile(Vector3 halfExtent)
        {
            if (m_CurrentTile == null)
            {
                m_CurrentTile = new GameObject("ProductionTile");
                m_CurrentTile.AddComponent<MeshFilter>();
                m_CurrentTile.GetComponent<MeshFilter>().mesh = CreateMesh(halfExtent);

                MeshRenderer meshRenderer = m_CurrentTile.AddComponent<MeshRenderer>();
                meshRenderer.material.shader = Shader.Find("UI/Default");
                meshRenderer.material.color = Color.white;
            }
            else
            {
                m_CurrentTile.SetActive(true);
                m_CurrentTile.GetComponent<MeshFilter>().mesh = CreateMesh(halfExtent);
            }
        }

        private Mesh CreateMesh(Vector3 halfExtent)
        {
            Mesh mesh = new Mesh();
            mesh.name = "rectangle";
            mesh.vertices = new Vector3[] { new Vector3(-halfExtent.x, 0, -halfExtent.z), new Vector3(-halfExtent.x, 0, halfExtent.z), new Vector3(halfExtent.x, 0, halfExtent.z), new Vector3(halfExtent.x, 0, -halfExtent.z) };
            mesh.uv = new Vector2[] { new Vector2(0, 0), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0) };
            mesh.triangles = new int[] { 0, 1, 2, 0, 2, 3 };
            return mesh;
        }
    }
}
