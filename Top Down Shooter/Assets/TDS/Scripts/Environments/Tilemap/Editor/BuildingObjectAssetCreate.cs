﻿using UnityEditor;
using VuxtaStudio.Common.Editor;
using VuxtaStudio.TDS.Environment;

namespace VuxtaStudio.TDS.Environment.Editor
{
    static class EnvironmentAssetCreate
    {
        [MenuItem("Assets/Create/TDS/Environment/BuildingObject")]
        public static void CreateBuildingObjectAsset()
        {
            ScriptableObjectUtility.CreateAsset<BuildingObject>();
        }

        [MenuItem("Assets/Create/TDS/Environment/BuildingListProfile")]
        public static void CreateBuildingListProfileAsset()
        {
            ScriptableObjectUtility.CreateAsset<BuildingListProfile>();
        }
    }
}
