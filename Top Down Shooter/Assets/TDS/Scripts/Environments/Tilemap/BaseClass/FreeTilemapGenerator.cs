﻿using UnityEngine;
using VuxtaStudio.TDS.Datas;
using VuxtaStudio.TDS.Global.Defines;
using VuxtaStudio.Common.Utils;
using System.Collections.Generic;
using System.Collections;
using System;
using System.Linq;

namespace VuxtaStudio.TDS.Environment
{
    public class FreeTilemapGenerator : MonoBehaviour
    {
        [SerializeField]
        protected Grid m_Grid;

        [SerializeField]
        protected BuildingListProfile m_BuildingListProfile;

        [SerializeField]
        protected int m_SpawnedPerFrame = 10;

        protected EnvironmentData m_CurrentEnvironment;

        protected string m_DefaultSavedPath;
        protected string m_DefaultLoadPath;

        protected Dictionary<GameObject, BuildingData> m_SpawnedBuildings = new Dictionary<GameObject, BuildingData>();
        protected Dictionary<GameObject, Vector3> m_SpawnedSurroundings = new Dictionary<GameObject, Vector3>();
        protected int[,] m_Map;

        private Collider[] m_Overlaps = new Collider[16];

        private void Awake()
        {
            m_DefaultSavedPath = FilePaths.EnvironmentFile;
            m_DefaultLoadPath = FilePaths.EnvironmentFile;
        }

        private IEnumerator LoadAsyncCoroutine(EnvironmentData newData, Action loadCompleteCallback)
        {
            int loadCount = 0;
            YieldInstruction yieldInstruction = new WaitForEndOfFrame();

            Debug.Log("Clean existing environment");

            if (m_SpawnedBuildings != null || m_SpawnedBuildings.Count > 0)
            {
                GameObject[] gameObjects = m_SpawnedBuildings.Keys.ToArray();
                for (int i = 0; i < gameObjects.Length; i++)
                {
                    if (RemoveObject(gameObjects[i]))
                    {
                        ++loadCount;
                    }

                    if (loadCount >= m_SpawnedPerFrame)
                    {
                        loadCount = 0;
                        yield return yieldInstruction;
                    }
                }

                yield return yieldInstruction;
            }

            Debug.Log("Remove existing surroundings");

            if (m_SpawnedSurroundings != null || m_SpawnedSurroundings.Count > 0)
            {
                GameObject[] gameObjects = m_SpawnedSurroundings.Keys.ToArray();
                for (int i = 0; i < gameObjects.Length; i++)
                {
                    if (RemoveSurrounding(gameObjects[i]))
                    {
                        ++loadCount;
                    }

                    if (loadCount >= m_SpawnedPerFrame)
                    {
                        loadCount = 0;
                        yield return yieldInstruction;
                    }
                }

                yield return yieldInstruction;
            }

            Debug.Log("Start loading environment");

            for (int i = 0; i < newData.Buildings.Count; i++)
            {
                BuildingObject buildingObject =
                    m_BuildingListProfile.BuildingList.Find(x => x.BuildingId == newData.Buildings[i].BuildingId);

                if (buildingObject == null)
                {
                    Debug.LogWarningFormat("Building ID {0}: does not exist in the profile, skipping...");
                    continue;
                }

                if (PlaceObject(newData.Buildings[i].Position, buildingObject))
                {
                    ++loadCount;
                }

                if (loadCount >= m_SpawnedPerFrame)
                {
                    loadCount = 0;
                    yield return yieldInstruction;
                }
            }

            Debug.Log("Start loading surroundings");

            yield return GenerateFromSurroundingDataAsync(newData.Surroundings);

            Debug.Log("Done loading");

            loadCompleteCallback?.Invoke();
        }

        public void ReloadSurroundings(SurroundingData data, Action completeCallback = null)
        {
            StartCoroutine(ReloadSurroundingsCoroutine(data, completeCallback));
        }

        private IEnumerator ReloadSurroundingsCoroutine(SurroundingData data, Action completeCallback)
        {
            int loadCount = 0;
            YieldInstruction yieldInstruction = new WaitForEndOfFrame();

            Debug.Log("Remove existing surroundings");

            if (m_SpawnedSurroundings != null || m_SpawnedSurroundings.Count > 0)
            {
                GameObject[] gameObjects = m_SpawnedSurroundings.Keys.ToArray();
                for (int i = 0; i < gameObjects.Length; i++)
                {
                    if (RemoveSurrounding(gameObjects[i]))
                    {
                        ++loadCount;
                    }

                    if (loadCount >= m_SpawnedPerFrame)
                    {
                        loadCount = 0;
                        yield return yieldInstruction;
                    }
                }

                yield return yieldInstruction;
            }

            Debug.Log("Start loading surroundings");

            yield return GenerateFromSurroundingDataAsync(data);

            Debug.Log("Complete load surroundings");

            completeCallback?.Invoke();
        }

        private IEnumerator GenerateFromSurroundingDataAsync(SurroundingData data)
        {
            if (data.IsSeeded
                && data.FillPercent != 0
                && data.Width != 0f
                && data.Length != 0f)
            {
                Debug.Log("Load surroundings from seed");
                yield return GenerateSurroundingFromSeedAsync(data);
            }

            if (!data.IsSeeded
                && data.PositionList != null
                && data.PositionList.Count > 0)
            {
                Debug.Log("Load surroundings from list");
                yield return GenerateFromSurroundListAsync(data);
            }
        }

        private IEnumerator GenerateFromSurroundListAsync(SurroundingData data)
        {
            int loadCount = 0;
            YieldInstruction yieldInstruction = new WaitForEndOfFrame();

            for (int i = 0; i < data.PositionList.Count; i++)
            {
                if (PlaceSurrounding(data.PositionList[i], m_BuildingListProfile.SurroundingObject))
                    ++loadCount;

                if (loadCount >= m_SpawnedPerFrame)
                {
                    loadCount = 0;
                    yield return yieldInstruction;
                }
            }

            yield return yieldInstruction;
        }

        private IEnumerator GenerateSurroundingFromSeedAsync(SurroundingData data)
        {
            int loadCount = 0;
            YieldInstruction yieldInstruction = new WaitForEndOfFrame();

            m_Map = ProceduralPattern.GenerateCellularAutomataMooreSmoothed((int)data.Length, (int)data.Width, data.Seed, data.FillPercent, data.EdgesAreWalls, data.SmoothAmount);
            Vector3 currentPlacementPosition = Vector3.zero;

            float m_MinX = data.CenterPosition.x - data.Length / 2;
            float m_MaxX = data.CenterPosition.x + data.Length / 2;
            float m_MinZ = data.CenterPosition.z - data.Width / 2;
            float m_MaxZ = data.CenterPosition.z + data.Width / 2;

            bool result = true;

            for (int i = 0; i < m_Map.GetUpperBound(0); i++)
            {
                currentPlacementPosition.x = m_MinX + i;
                for (int j = 0; j < m_Map.GetUpperBound(1); j++)
                {
                    currentPlacementPosition.z = m_MinZ + j;
                    if (m_Map[i, j] == 1)
                    {
                        if (PlaceSurrounding(currentPlacementPosition, m_BuildingListProfile.SurroundingObject))
                        {
                            ++loadCount;
                        }
                        else
                        {
                            result = false;
                        }
                    }

                    if (loadCount >= m_SpawnedPerFrame)
                    {
                        loadCount = 0;
                        yield return yieldInstruction;
                    }
                }
            }

            m_CurrentEnvironment.Surroundings.Seed = data.Seed;
            m_CurrentEnvironment.Surroundings.CenterPosition = data.CenterPosition;
            m_CurrentEnvironment.Surroundings.FillPercent = data.FillPercent;
            m_CurrentEnvironment.Surroundings.Width = data.Width;
            m_CurrentEnvironment.Surroundings.Length = data.Length;
            m_CurrentEnvironment.Surroundings.SmoothAmount = data.SmoothAmount;
            m_CurrentEnvironment.Surroundings.EdgesAreWalls = data.EdgesAreWalls;

            if (result)
            {
                m_CurrentEnvironment.Surroundings.IsSeeded = data.IsSeeded;
            }
            else
            {
                m_CurrentEnvironment.Surroundings.IsSeeded = false;
            }

            yield return yieldInstruction;
        }

        public BuildingListProfile GetProfile()
        {
            return m_BuildingListProfile;
        }

        public void SetSavedPath(string newPath)
        {
            m_DefaultSavedPath = newPath;
        }

        public void SetLoadPath(string newPath)
        {
            m_DefaultLoadPath = newPath;
        }

        public bool SaveEnvironmentData()
        {
            EnvironmentData savedEnvironment = m_CurrentEnvironment;

            if (savedEnvironment.Surroundings.IsSeeded
                && savedEnvironment.Surroundings.FillPercent != 0
                && savedEnvironment.Surroundings.Width != 0f
                && savedEnvironment.Surroundings.Length != 0f)
            {
                Debug.Log("Compress surrounding data as seeding data");
                savedEnvironment.Surroundings.PositionList.Clear();
            }
            else
            {
                savedEnvironment.Surroundings.IsSeeded = false;
            }

            return FileHandler.WriteTo(savedEnvironment.SaveToJson(), m_DefaultSavedPath);
        }

        public bool LoadEnvironmentData(Action loadCompleteCallback = null)
        {
            string jsonString = FileHandler.ReadFrom(m_DefaultLoadPath);

            if (string.IsNullOrEmpty(jsonString))
            {
                Debug.LogWarning("Load from file failed");
                return false;
            }

            EnvironmentData newData = JsonUtility.FromJson<EnvironmentData>(jsonString);

            if (newData == null || newData.Buildings == null || newData.Buildings.Count <= 0)
            {
                Debug.LogWarning("Cannot parse data");
                return false;
            }

            StartCoroutine(LoadAsyncCoroutine(newData, loadCompleteCallback));

            return true;
        }

        public Vector3 SnapToGrid(Vector3 pos)
        {
            return GridToWorld(ToGridPosition(pos));
        }

        public Vector3Int ToGridPosition(Vector3 pos)
        {
            return m_Grid.WorldToCell(pos);
        }

        public Vector3 GridToWorld(Vector3Int pos)
        {
            return m_Grid.CellToWorld(pos);
        }

        public bool PlaceSurrounding(Vector3 pos, BuildingObject obj, bool snapToGrid = false)
        {
            Vector3 newPos = pos;

            if (snapToGrid)
            {
                newPos = SnapToGrid(pos);
            }

            if (HasObject(newPos, obj))
            {
                Debug.LogWarningFormat("Fail to set surrounding tile, tile already exist at {0}", newPos);
                return false;
            }

            GameObject newBulding = Instantiate(obj.BuildingPrefabs, newPos, Quaternion.identity);

            m_SpawnedSurroundings.Add(newBulding, newPos);

            InitEnvironmentData();

            m_CurrentEnvironment.Surroundings.PositionList.Add(newPos);

            return true;
        }

        public bool RemoveSurrounding(GameObject obj)
        {
            Vector3 buildingData;

            if (m_SpawnedSurroundings.TryGetValue(obj, out buildingData))
            {
                m_CurrentEnvironment.Surroundings.PositionList.Remove(buildingData);
                m_CurrentEnvironment.Surroundings.IsSeeded = false;
                m_SpawnedSurroundings.Remove(obj);
                Destroy(obj);
                return true;
            }
            else
            {
                Debug.LogWarning("Object not in the surrounding list, abort...");
                return false;
            }
        }

        public bool PlaceObject(Vector3 pos, BuildingObject obj, bool snapToGrid = false)
        {
            Vector3 newPos = pos;

            if (snapToGrid)
            {
                newPos = SnapToGrid(pos);
            }

            if (HasObject(newPos, obj))
            {
                Debug.LogWarningFormat("Fail to set building tile, tile already exist at {0}", newPos);
                return false;
            }

            GameObject newBulding = Instantiate(obj.BuildingPrefabs, newPos, Quaternion.identity);

            BuildingData newBuildingData = new BuildingData();
            newBuildingData.Position = newPos;
            newBuildingData.BuildingId = obj.BuildingId;

            m_SpawnedBuildings.Add(newBulding, newBuildingData);

            InitEnvironmentData();

            m_CurrentEnvironment.Buildings.Add(newBuildingData);

            return true;
        }

        private void InitEnvironmentData()
        {
            if (m_CurrentEnvironment == null)
            {
                m_CurrentEnvironment = new EnvironmentData();
            }

            if (m_CurrentEnvironment.Buildings == null)
            {
                m_CurrentEnvironment.Buildings = new List<BuildingData>();
            }

            if (m_CurrentEnvironment.Surroundings == null)
            {
                m_CurrentEnvironment.Surroundings = new SurroundingData();
            }

            if (m_CurrentEnvironment.Surroundings.PositionList == null)
            {
                m_CurrentEnvironment.Surroundings.PositionList = new List<Vector3>();
                m_CurrentEnvironment.Surroundings.IsSeeded = false;
            }
        }

        public bool HasObject(Vector3 pos, BuildingObject obj)
        {
            Vector3 halfExtents = obj.HalfExtents;

            bool result = (Physics.OverlapBoxNonAlloc(pos, halfExtents, m_Overlaps, Quaternion.identity, 1 << obj.BuildingPrefabs.layer) > 0);

#if UNITY_EDITOR
            VuxtaStudio.Common.Extensions.DebugExtension.DrawBox(pos, halfExtents, Quaternion.identity, result ? Color.red : Color.green);
#endif
            return result;
        }

        public bool RemoveObject(GameObject obj)
        {
            BuildingData buildingData;

            if (m_SpawnedBuildings.TryGetValue(obj, out buildingData))
            {
                m_CurrentEnvironment.Buildings.Remove(buildingData);
                m_SpawnedBuildings.Remove(obj);
                Destroy(obj);
                return true;
            }
            else
            {
                Debug.LogWarning("Object not in the building list, abort...");
                return false;
            }
        }
    }
}
