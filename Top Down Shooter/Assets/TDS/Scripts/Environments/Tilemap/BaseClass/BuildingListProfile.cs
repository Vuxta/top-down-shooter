﻿using UnityEngine;
using System.Collections.Generic;

namespace VuxtaStudio.TDS.Environment
{
    public class BuildingListProfile : ScriptableObject
    {
        public List<BuildingObject> BuildingList;
        public BuildingObject SurroundingObject;
    }
}
