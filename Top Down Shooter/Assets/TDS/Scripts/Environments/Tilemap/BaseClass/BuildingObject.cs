﻿using UnityEngine;

namespace VuxtaStudio.TDS.Environment
{
    public class BuildingObject : ScriptableObject
    {
        public GameObject BuildingPrefabs;
        public Vector3 HalfExtents;
        public int BuildingId;
    }
}
