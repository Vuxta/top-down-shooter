﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace VuxtaStudio.TDS.Environment
{
    public abstract class TilemapGeneratorBase : MonoBehaviour
    {
        [SerializeField]
        protected Grid m_Grid;

        [SerializeField]
        protected Tilemap m_Foreground;

        protected Vector3Int ToGridPosition(Vector3 pos)
        {
            return m_Grid.WorldToCell(pos);
        }

        protected bool SetTile(Vector3 pos, TileBase tile)
        {
            Vector3Int gridPos = ToGridPosition(pos);

            return SetTile(gridPos, tile);
        }

        protected bool SetTile(Vector3Int pos, TileBase tile)
        {
            if (HasTile(pos))
            {
                Debug.LogWarningFormat("Fail to set tile, tile already exist at {0}", pos);
                return false;
            }

            m_Foreground.SetTile(pos, tile);

            return true;
        }

        protected bool HasTile(Vector3Int pos)
        {
            return m_Foreground.HasTile(pos);
        }

        protected bool RemoveTile(Vector3Int pos)
        {
            m_Foreground.SetTile(pos, null);

            return true;
        }
    }
}
