﻿using UnityEngine;
using VuxtaStudio.TDS.AIControl;

using VuxtaStudio.TDS.WeaponSystem;

namespace VuxtaStudio.TDS.Sandbox
{
    public class WatchTower : AIController
    {
        public FireModule m_FireModule;

        [SerializeField]
        private float m_Angle = 360f;

        [SerializeField]
        private float m_Distance = 20f;

        protected override void SensorsInit()
        {
            m_Sensors = new SensorBase[] { new SphereCastSensor(m_FireModule.transform, m_SensorLayers, m_Angle, m_Distance, m_TargetTags) };
        }

        protected override void StatesInit()
        {
            m_Idle = new DummyIdleState(this, OnIdleTargetInsight);
            m_Inspect = new DummyInspectState(this, OnInspectTargetLost, OnInspectTargetInRange);
            m_Attack = new DummyAttackState(this, OnAttackTargetOutOfRange);
        }

        public override void FireAt(Vector3 pos)
        {
            m_FireModule.FireAt(pos);
        }

    }
}
