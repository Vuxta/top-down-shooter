﻿using UnityEngine;
using VuxtaStudio.Common.InputSystems;
using VuxtaStudio.TDS.PlayerControl;
using VuxtaStudio.TDS.WeaponSystem;

namespace VuxtaStudio.TDS.Sandbox
{
    /// <summary>
    /// Game manager for prototyping
    /// </summary>
    public class PrototypeGameManager : MonoBehaviour
    {
        [SerializeField]
        private PlayerController m_PlayerController;

        [SerializeField]
        private PlayerMovementComponent m_MovementComponent;

        [SerializeField]
        private VirtualJoystickAxisComponent m_VirtualDirectionAxisHandler;

        [SerializeField]
        private VirtualJoystickAxisComponent m_VirtualAimAxisHandler;

        [SerializeField]
        private VirtualButtonComponent m_VirtualAttackButtonHandler;

        [SerializeField]
        private VirtualButtonComponent m_VirtualReloadButtonHandler;

        [SerializeField]
        private VirtualButtonComponent m_VirtualSwitchButtonHandler;

        [SerializeField]
        private PlayerAttackComponent m_AttackComponent;

        [SerializeField]
        private FireModule m_FireModule;

        [SerializeField]
        private bool m_UseDesktopInputs = true;

        [SerializeField]
        private GameObject[] m_UIObjects;

        private InputSystem m_InputSystem;

#if UNITY_EDITOR || VUXTASTUDIO_DEBUG
        private void Awake()
        {
            InputSystemProfile inputProfile;

#if UNITY_ANDROID || UNITY_IPHONE
            m_UseDesktopInputs = false;
#endif
            if (m_UseDesktopInputs)
            {
                inputProfile = new DesktopInputProfile(m_MovementComponent.GetTransform());

                if (m_UIObjects != null)
                {
                    for (int i = 0; i < m_UIObjects.Length; i++)
                    {
                        m_UIObjects[i].SetActive(false);
                    }
                }
            }
            else
            {
                inputProfile = new DefaultInputProfile(m_VirtualDirectionAxisHandler,
                                                        m_VirtualAimAxisHandler,
                                                        m_VirtualAttackButtonHandler,
                                                        m_VirtualReloadButtonHandler,
                                                        m_VirtualSwitchButtonHandler);
            }

            //Init input systems
            if (InputSystemManager.Instance.RegisterUser(GetInstanceID(), out m_InputSystem, inputProfile))
            {
                Debug.LogFormat("[PrototypeGameManager::AssignDebugUser] auto assign debug user");
            }

            m_PlayerController.SetInputSystem(m_InputSystem);
            m_PlayerController.SetMovementComponent(m_MovementComponent);

            //Init weapons controls
            m_AttackComponent.OnAttackButtonPressed += m_FireModule.Fire;
            m_AttackComponent.OnReloadButtonDown += m_FireModule.Reload;
            m_AttackComponent.OnSwitchButtonDown += m_FireModule.Switch;
            m_PlayerController.SetAttackComponent(m_AttackComponent);
        }
#endif
    }
}
