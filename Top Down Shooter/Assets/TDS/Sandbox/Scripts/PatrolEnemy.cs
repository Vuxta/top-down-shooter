﻿using UnityEngine;
using VuxtaStudio.TDS.AIControl;
using VuxtaStudio.TDS.WeaponSystem;
using VuxtaStudio.TDS.SteeringSystem;

namespace VuxtaStudio.TDS.Sandbox
{
    public class PatrolEnemy : AIController
    {
        [SerializeField]
        private FireModule m_FireModule;

        [SerializeField]
        private Vector3[] m_PatrolPath;

        [SerializeField]
        private float m_Angle = 60f;

        [SerializeField]
        private float m_Distance = 10f;

        public override void FireAt(Vector3 pos)
        {
            m_FireModule.FireAt(pos);
        }

        public void SetPath(Vector3[] paths)
        {
            m_PatrolPath = paths;

            if (m_Idle != null)
            {
                ((IdlePatrolAndLookAroundState)m_Idle).SetPatrolRoute(m_PatrolPath);
            }
        }

        protected override void SensorsInit()
        {
            m_Sensors = new SensorBase[] { new SphereCastSensor(m_FireModule.transform, m_SensorLayers, m_Angle, m_Distance, m_TargetTags) };
        }

        protected override void StatesInit()
        {
            m_Idle = new IdlePatrolAndLookAroundState(this, OnIdleTargetInsight, m_PatrolPath);
            m_Inspect = new InspectSearchState(this, OnInspectTargetLost, OnInspectTargetInRange);
            m_Attack = new AttackStaticState(this, OnAttackTargetOutOfRange);
        }

        private void OnDrawGizmosSelected()
        {
            if (m_PatrolPath == null || m_PatrolPath.Length <= 0)
                return;

            for (int i = 0; i < m_PatrolPath.Length; i++)
            {
                Gizmos.DrawSphere(m_PatrolPath[i], 1f);

                if (i == m_PatrolPath.Length - 1)
                {
                    Gizmos.DrawLine(m_PatrolPath[i], m_PatrolPath[0]);
                }
                else
                {
                    Gizmos.DrawLine(m_PatrolPath[i], m_PatrolPath[i + 1]);
                }
            }
        }
    }
}
