﻿using UnityEngine;
using UnityEngine.Tilemaps;
using VuxtaStudio.TDS.Environment;

namespace VuxtaStudio.TDS.Sandbox
{
    public class RandomTilemapGenerator : TilemapGeneratorBase
    {
        [SerializeField]
        private Vector3 m_CenterPosition = Vector3.zero;

        [Tooltip("Distance in X axis")]
        [SerializeField]
        private float m_Length = 10f;

        [Tooltip("Distance in Z axis")]
        [SerializeField]
        private float m_Width = 10f;

        [SerializeField]
        private int m_Counts = 10;

        [SerializeField]
        private TileBase m_Tile;

        private void Start()
        {
            float minX = m_CenterPosition.x - m_Length / 2;
            float maxX = m_CenterPosition.x + m_Length / 2;
            float minZ = m_CenterPosition.z - m_Length / 2;
            float maxZ = m_CenterPosition.z + m_Length / 2;

            for (int i = 0; i < m_Counts; i++)
            {
                Vector3 pos = PickRandom(minX, maxX, minZ, maxZ);
                SetTile(pos, m_Tile);
            }
        }

        private Vector3 PickRandom(float minX, float maxX, float minZ, float maxZ)
        {
            return new Vector3(Random.Range(minX, maxX), 0f, Random.Range(minZ, maxZ));
        }
    }
}
