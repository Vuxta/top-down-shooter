﻿using UnityEngine;
using System.Collections.Generic;
using VuxtaStudio.Common.Extensions;
using VuxtaStudio.TDS.PhysicsSystem;

namespace VuxtaStudio.TDS.Sandbox
{
    public class RallyPointSpawner : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] m_SpawnablePrefabs;

        [SerializeField]
        private float m_TimeToSpawn = 3f;

        [SerializeField]
        private int m_MaxSpawns = 1;

        private float m_SpawnTimeCounter = 0f;
        private RallyPointSpawner[] m_RallyPoints;
        private List<Vector3> m_RallyPositions = new List<Vector3>();

        private int m_CurrentSpawns = 0;

        // just for prototypes, this should use events to update the rally point instead
        private void Update()
        {
            m_SpawnTimeCounter += Time.deltaTime;

            if (m_SpawnTimeCounter > m_TimeToSpawn
                && m_CurrentSpawns < m_MaxSpawns)
            {
                m_SpawnTimeCounter = 0f;
                m_RallyPoints = (RallyPointSpawner[])FindObjectsOfType(typeof(RallyPointSpawner));

                //just go random...
                m_RallyPositions.Clear();

                for (int i = 0; i < m_RallyPoints.Length; i++)
                {
                    m_RallyPositions.Add(m_RallyPoints[i].transform.position);
                }

                m_RallyPositions = ListExtension.ShuffleList(m_RallyPositions);
                GameObject spawnedObject = Instantiate(m_SpawnablePrefabs[Random.Range(0, m_SpawnablePrefabs.Length)], this.transform);

                //TODO: set path to spawned objects
                PatrolEnemy patrolEnemy;
                if (spawnedObject.TryGetComponent<PatrolEnemy>(out patrolEnemy))
                {
                    patrolEnemy.SetPath(m_RallyPositions.ToArray());
                }

                DamagableComponent patrolEnemyDamagable;
                if (spawnedObject.TryGetComponent<DamagableComponent>(out patrolEnemyDamagable))
                {
                    ++m_CurrentSpawns;
                    patrolEnemyDamagable.OnDie.AddListener(OnSpawnedObjectDie);
                }
            }
        }

        private void OnSpawnedObjectDie(DamageInfo arg0)
        {
            --m_CurrentSpawns;
        }
    }
}
