﻿using System;
using UnityEngine;
using VuxtaStudio.TDS.PhysicsSystem;

namespace VuxtaStudio.TDS.Sandbox
{
    [RequireComponent(typeof(DamagableComponent))]
    public class SandboxDestroyableBrick : MonoBehaviour
    {
        private DamagableComponent m_DamagableComponent;

        private void Start()
        {
            m_DamagableComponent = GetComponent<DamagableComponent>();
            m_DamagableComponent.OnDie.AddListener(OnDieEvent);
        }

        /// <summary>
        /// Callback event when enemy dies
        /// </summary>
        /// <param name="info">info abour damage</param>
        private void OnDieEvent(DamageInfo info)
        {
            Destroy(gameObject);
        }
    }
}