﻿using VuxtaStudio.TDS.Environment;
using UnityEngine;
using System.IO;
using VuxtaStudio.TDS.Global.Defines;
using VuxtaStudio.TDS.Datas;
using System.Collections.Generic;
using System.Collections;

namespace VuxtaStudio.TDS.Sandbox
{
    public class RandomFreeTilemapGenerator : FreeTilemapGenerator
    {
        [SerializeField]
        private Vector3 m_CenterPosition = Vector3.zero;

        [Tooltip("Distance in X axis")]
        [SerializeField]
        private float m_Length = 10f;

        [Tooltip("Distance in Z axis")]
        [SerializeField]
        private float m_Width = 10f;

        [SerializeField]
        private int m_RallyPointCounts = 10;

        [SerializeField]
        private bool m_SnapToGrid = true;

        [SerializeField]
        private string m_LoadPath;

        [SerializeField]
        private bool m_RandomSeed = true;

        [SerializeField]
        private float m_Seed = 1.215f;

        [SerializeField]
        private bool m_EdgesAreWalls = false;

        [SerializeField]
        private int m_FillPercent = 45;

        [SerializeField]
        private int m_SmoothAmount = 40;

        private List<Vector3> m_Corners = new List<Vector3>();

        private void Start()
        {
            GenerateSurroundings();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.L))
            {
                SetLoadPath(Path.Combine(FilePaths.EnvironmentDirectory, m_LoadPath));
                LoadEnvironmentData();
            }
        }

        private void GenerateSurroundings()
        {
            float seed = (m_RandomSeed) ? System.Guid.NewGuid().GetHashCode() : m_Seed;
            SurroundingData data = new SurroundingData();

            Debug.LogFormat("Generate surroundings by seed: {0}", seed);

            data.IsSeeded = true;
            data.Length = m_Length;
            data.Width = m_Width;
            data.Seed = seed;
            data.FillPercent = m_FillPercent;
            data.EdgesAreWalls = m_EdgesAreWalls;
            data.SmoothAmount = m_SmoothAmount;

            ReloadSurroundings(data, OnGenerateSurroundingComplete);
        }

        private void OnGenerateSurroundingComplete()
        {
            CalculateCorners(m_CurrentEnvironment.Surroundings.CenterPosition,
                            m_CurrentEnvironment.Surroundings.Length,
                            m_CurrentEnvironment.Surroundings.Width,
                            m_CurrentEnvironment.Surroundings.EdgesAreWalls);

            StartCoroutine(PlaceObjects());
        }

        private IEnumerator PlaceObjects()
        {
            int loadCount = 0;
            YieldInstruction yieldInstruction = new WaitForSeconds(0.5f);

            //place rally spawn point;
            int i = 0;

            System.Random r = new System.Random(System.Guid.NewGuid().GetHashCode());

            if (m_Corners.Count > m_RallyPointCounts)
            {
                i += r.Next(1, (m_Corners.Count / m_RallyPointCounts));
            }
            else
            {
                i += r.Next(1, 4);
            }

            while (i < m_Corners.Count)
            {
                if (PlaceObject(m_Corners[i], m_BuildingListProfile.BuildingList[2], m_SnapToGrid))
                {
                    ++loadCount;

                    if (m_Corners.Count > m_RallyPointCounts)
                    {
                        i += r.Next(1 + (m_Corners.Count / m_RallyPointCounts) / 2, (m_Corners.Count / m_RallyPointCounts));
                    }
                    else
                    {
                        i += r.Next(1, 4);
                    }

                }

                if (loadCount > 5)
                {
                    loadCount = 0;
                    yield return yieldInstruction;
                }
            }

            yield return new WaitForEndOfFrame();

            SaveEnvironmentData();
        }

        private Vector3 PickRandom()
        {
            return m_Corners[Random.Range(0, m_Corners.Count)];
        }

        private void CalculateCorners(Vector3 centerPosition, float length, float width, bool edgesAreWalls)
        {
            float minX = centerPosition.x - length / 2;
            float maxX = centerPosition.x + length / 2;
            float minZ = centerPosition.z - width / 2;
            float maxZ = centerPosition.z + width / 2;

            Vector3 currentPlacementPosition = Vector3.zero;
            m_Corners.Clear();

            for (int i = 0; i < m_Map.GetUpperBound(0); i++)
            {
                currentPlacementPosition.x = minX + i;
                for (int j = 0; j < m_Map.GetUpperBound(1); j++)
                {
                    currentPlacementPosition.z = minZ + j;
                    if ((m_Map[i, j] == 0)
                        && (ProceduralPattern.GetMooreSurroundingTiles(m_Map, i, j, edgesAreWalls) > 3))
                    {
                        m_Corners.Add(currentPlacementPosition);
                    }
                }
            }
        }
    }
}
