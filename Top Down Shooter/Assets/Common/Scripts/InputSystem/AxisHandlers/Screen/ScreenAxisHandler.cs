﻿using UnityEngine;

namespace VuxtaStudio.Common.InputSystems
{
    public class ScreenAxisHandler : IAxisHandler
    {
        private Vector2 m_MousePos;
        private int m_ScreenWidth;
        private int m_ScreenHeight;

        public float GetHorizontalAxis()
        {
            return GetXYAxis().y;
        }

        public float GetVerticalAxis()
        {
            return GetXYAxis().x;
        }

        public Vector2 GetXYAxis()
        {
            m_MousePos = Input.mousePosition;
            m_ScreenWidth = Screen.width / 2;
            m_ScreenHeight = Screen.height / 2;

            m_MousePos.x -= m_ScreenWidth;
            m_MousePos.y -= m_ScreenHeight;

            m_MousePos.x = m_MousePos.x / m_ScreenWidth;
            m_MousePos.y = m_MousePos.y / m_ScreenHeight;

            return m_MousePos;
        }
    }
}
