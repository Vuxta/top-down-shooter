﻿using UnityEngine;

namespace VuxtaStudio.Common.InputSystems
{
    public class TransformAxisHandler : IAxisHandler
    {
        private Vector2 m_CurrentResult = Vector2.zero;
        private Vector3 m_MousePos;
        private Transform m_CenterTransform;

        public TransformAxisHandler(Transform centerTransform)
        {
            m_CenterTransform = centerTransform;
        }

        public float GetHorizontalAxis()
        {
            return GetXYAxis().x;
        }

        public float GetVerticalAxis()
        {
            return GetXYAxis().y;
        }

        public Vector2 GetXYAxis()
        {
            m_MousePos = Input.mousePosition;
            m_MousePos.z = Camera.main.transform.position.y - m_CenterTransform.position.y;
            m_MousePos = Camera.main.ScreenToWorldPoint(m_MousePos);
            m_CurrentResult.x = m_MousePos.x - m_CenterTransform.position.x;
            m_CurrentResult.y = m_MousePos.z - m_CenterTransform.position.z;

            //m_CurrentResult.x = Mathf.Clamp(m_CurrentResult.x, -1f, 1f);
            //m_CurrentResult.y = Mathf.Clamp(m_CurrentResult.y, -1f, 1f);

            m_CurrentResult.Normalize();

            return m_CurrentResult;
        }
    }
}
