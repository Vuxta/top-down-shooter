﻿// <copyright file="ButtonAxisComponent.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>12/11/2018 10:53:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, December 11, 2018

using UnityEngine;

namespace VuxtaStudio.Common.InputSystems
{
    /// <summary>
    /// Axis handler for simulating axis control by the desired button handlers.
    /// This is a MonoBehaviour component, and this will simulate axis drags in the update calls
    /// Perfect for simulating joystick axis control by using keyboard or other buttons
    /// For simpler handler see SimpleButtonAxisHandler.cs
    /// </summary>
    public class ButtonAxisComponent : MonoBehaviour, IAxisHandler
    {
        #region Private Fields

        private IButtonHandler[] m_XAxisPositiveHandlers;
        private IButtonHandler[] m_XAxisNegativeHandlers;
        private IButtonHandler[] m_YAxisPositiveHandlers;
        private IButtonHandler[] m_YAxisNegativeHandlers;

        private Vector2 m_VectorAxis;
        private float m_HorizontalValue;
        private float m_VerticalValue;

        private float m_SmoothDuration = 0.35f;
        private float m_SmoothStepsPerUpdate = 0f;

        #endregion

        #region Public Methods

        /// <summary>
        /// Assign all button handlers for different axis and positive/negative values
        /// </summary>
        /// <param name="XAxisPositiveHandlers">Button handlers represent positive values of Horizontal/X-Axis</param>
        /// <param name="XAxisNegativeHandlers">Button handlers represent negative values of Horizontal/X-Axis</param>
        /// <param name="YAxisPositiveHandlers">Button handlers represent positive values of Vertical/Y-Axis</param>
        /// <param name="YAxisNegativeHandlers">Button handlers represent negative values of Vertical/Y-Axis</param>
        public void Initialize(IButtonHandler[] XAxisPositiveHandlers,
                                IButtonHandler[] XAxisNegativeHandlers,
                                IButtonHandler[] YAxisPositiveHandlers,
                                IButtonHandler[] YAxisNegativeHandlers)
        {
            this.m_XAxisPositiveHandlers = XAxisPositiveHandlers;
            this.m_XAxisNegativeHandlers = XAxisNegativeHandlers;
            this.m_YAxisPositiveHandlers = YAxisPositiveHandlers;
            this.m_YAxisNegativeHandlers = YAxisNegativeHandlers;
        }

        /// <summary>
        /// Get horizontal/X-Axis input value
        /// </summary>
        /// <returns>Return from -1f to 1f</returns>
        public float GetHorizontalAxis()
        {
            return m_HorizontalValue;
        }

        /// <summary>
        /// Get vertical/Y-Axis input value
        /// </summary>
        /// <returns>Return from -1f to 1f</returns>
        public float GetVerticalAxis()
        {
            return m_VerticalValue;
        }

        /// <summary>
        /// Get XY-Axis input value
        /// </summary>
        /// <returns>Both X and Y value are from -1f to 1f</returns>
        public Vector2 GetXYAxis()
        {
            m_VectorAxis.x = GetHorizontalAxis();
            m_VectorAxis.y = GetVerticalAxis();

            return m_VectorAxis;
        }

        #endregion

        #region Unity MonoBehaviour Methods

        /// <summary>
        /// Unity MonoBehaviour OnEnable
        /// Set all values to zero
        /// </summary>
        private void OnEnable()
        {
            m_VectorAxis = Vector2.zero;
            m_HorizontalValue = 0f;
            m_VerticalValue = 0f;
        }

        /// <summary>
        /// Unity MonoBehaviour OnDisable
        /// Set all values to zero
        /// </summary>
        private void OnDisable()
        {
            m_VectorAxis = Vector2.zero;
            m_HorizontalValue = 0f;
            m_VerticalValue = 0f;
        }

        /// <summary>
        /// Unity MonoBehaviour Update
        /// Updates the draging steps value, horizontal/vertical value of this frame
        /// </summary>
        private void Update()
        {
            UpdateSmoothStep();
            UpdateHorizontalValue();
            UpdateVerticalValue();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Update smooth steps which will be used by updating values of each axis
        /// This is calculated by the delta time of update call devided by smooth duration
        /// </summary>
        private void UpdateSmoothStep()
        {
            m_SmoothStepsPerUpdate = Time.deltaTime / m_SmoothDuration;
        }

        /// <summary>
        /// Updates horizontal values of this frame
        /// </summary>
        private void UpdateHorizontalValue()
        {
            UpdateAxisValue(ref m_XAxisPositiveHandlers,
                            ref m_XAxisNegativeHandlers,
                            ref m_HorizontalValue);
        }

        /// <summary>
        /// Updates vertical values of this frame
        /// </summary>
        private void UpdateVerticalValue()
        {
            UpdateAxisValue(ref m_YAxisPositiveHandlers,
                            ref m_YAxisNegativeHandlers,
                            ref m_VerticalValue);
        }

        /// <summary>
        /// Detects all positive/negative button status and update axis values accordingly
        /// </summary>
        /// <param name="positiveHandlers">Button handlers represent positive axis input</param>
        /// <param name="negativeHandlers">Button handlers represent negative axis input</param>
        /// <param name="valueToUpdate">Value to be updated</param>
        private void UpdateAxisValue(ref IButtonHandler[] positiveHandlers,
                                    ref IButtonHandler[] negativeHandlers,
                                    ref float valueToUpdate)
        {
            bool positiveValue = GetPressedValue(ref positiveHandlers);
            bool negativeValue = GetPressedValue(ref negativeHandlers);
            float force = 0f;
            float clampMin = 0f;
            float clampMax = 0f;

            if (positiveValue == true
                && negativeValue == true)
            {
                return;
            }

            if (positiveValue == false
                && negativeValue == false)
            {
                if (valueToUpdate == 0f)
                    return;

                if (valueToUpdate < 0f)
                {
                    force = 1f;
                    clampMax = 0f;
                    clampMin = -1f;
                }
                else
                {
                    force = -1f;
                    clampMax = 1f;
                    clampMin = 0f;
                }
            }
            else if (positiveValue == true)
            {
                if (valueToUpdate == 1f)
                    return;

                force = 1f;
                clampMax = 1f;
                clampMin = 0f;
            }
            else if (negativeValue == true)
            {
                if (valueToUpdate == -1f)
                    return;

                force = -1f;
                clampMax = 0f;
                clampMin = -1f;
            }

            valueToUpdate += force * m_SmoothStepsPerUpdate;
            valueToUpdate = Mathf.Clamp(valueToUpdate, clampMin, clampMax);
        }

        /// <summary>
        /// Check if any of the button handlers returns pressed
        /// </summary>
        /// <param name="buttonHandlers">Button handlers to be checked</param>
        /// <returns>Return true if any of the button handler is pressed</returns>
        private bool GetPressedValue(ref IButtonHandler[] buttonHandlers)
        {
            if (buttonHandlers == null)
                return false;

            bool isPressed = false;
            for (int i = 0; i < buttonHandlers.Length; i++)
            {
                if (buttonHandlers[i] != null
                    && buttonHandlers[i].GetButtonPressed())
                {
                    isPressed = true;
                    break;
                }
            }

            return isPressed;
        }

        #endregion
    }
}
