﻿// <copyright file="KeyboardButtonHandler.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>12/11/2018 10:53:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, December 11, 2018

using UnityEngine;

namespace VuxtaStudio.Common.InputSystems
{
    /// <summary>
    /// Button handler for keyboard input device
    /// </summary>
    public class KeyboardButtonHandler : IButtonHandler
    {
        #region Private Fields

        private readonly KeyCode m_KeyCode;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor of keyboard handler
        /// </summary>
        /// <param name="keyCode">KeyCode to be handled</param>
        public KeyboardButtonHandler(KeyCode keyCode)
        {
            this.m_KeyCode = keyCode;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get key button down status from unity input
        /// </summary>
        /// <returns>Return true if button is down</returns>
        public bool GetButtonDown()
        {
            return Input.GetKeyDown(m_KeyCode);
        }

        /// <summary>
        /// Get key button pressed status from unity input
        /// </summary>
        /// <returns>Return true if button is pressed</returns>
        public bool GetButtonPressed()
        {
            return Input.GetKey(m_KeyCode);
        }

        /// <summary>
        /// Get key button up status from unity input
        /// </summary>
        /// <returns>Return true if button is up</returns>
        public bool GetButtonUp()
        {
            return Input.GetKeyUp(m_KeyCode);
        }

        #endregion
    }
}
