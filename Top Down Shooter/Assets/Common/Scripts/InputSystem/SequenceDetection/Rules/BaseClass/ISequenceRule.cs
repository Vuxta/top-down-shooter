﻿// <copyright file="ISequenceRule.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>12/21/2018 14:00:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, December 21, 2018

namespace VuxtaStudio.Common.InputSystems
{
    /// <summary>
    /// Interface of sequence rules
    /// </summary>
    public interface ISequenceRule
    {
        /// <summary>
        /// Assign input system at runtime.
        /// This will be useful for setting up sequence ahead without assigning the input system first.
        /// </summary>
        /// <param name="inputSystem">input system to detect</param>
        void AssignInputSystem(InputSystem inputSystem);

        /// <summary>
        /// Check if rule is matched
        /// </summary>
        /// <returns>Return true if sequence condition matched</returns>
        bool Match();
    }
}
