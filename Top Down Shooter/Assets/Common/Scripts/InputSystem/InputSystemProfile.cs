﻿// <copyright file="InputSystemProfile.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>12/11/2018 10:53:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, December 11, 2018

using UnityEngine;
using System.Collections.Generic;

namespace VuxtaStudio.Common.InputSystems
{
    /// <summary>
    /// Input system profile stores all button and axis handlers with paired ID
    /// </summary>
    public class InputSystemProfile
    {
        #region Private Fields

        protected Dictionary<int, IAxisHandler[]> m_AxisHandlerList = new Dictionary<int, IAxisHandler[]>();
        protected Dictionary<int, IButtonHandler[]> m_ButtonHandlerList = new Dictionary<int, IButtonHandler[]>();

        #endregion

        #region Public Fields

        public Dictionary<int, IAxisHandler[]> AxisHandlerList
        {
            get
            {
                return m_AxisHandlerList;
            }
        }

        public Dictionary<int, IButtonHandler[]> ButtonHandlerList
        {
            get
            {
                return m_ButtonHandlerList;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Registered axis handlers with an axis ID
        /// </summary>
        /// <param name="id">Axis id registered in the profile</param>
        /// <param name="axisHandler">Array of axis handlers</param>
        /// <returns>Return true if successfully registered</returns>
        public bool RegisterAxisHandler(int id, params IAxisHandler[] axisHandler)
        {
            if (m_AxisHandlerList.ContainsKey(id))
            {
                Debug.LogWarningFormat("[InputSystemProfile::RegisterAxisHandler] id: {0} is already registered, abort...");
                return false;
            }

            m_AxisHandlerList.Add(id, axisHandler);
            return true;
        }

        /// <summary>
        /// Un-registered axis handlers with an axis ID
        /// </summary>
        /// <param name="id">Axis id registered in the profile</param>
        /// <returns>Return true if successfully un-registered</returns>
        public bool UnRegisterAxisHandler(int id)
        {
            if (m_AxisHandlerList.ContainsKey(id) == false)
            {
                Debug.LogWarningFormat("[InputSystemProfile::UnRegisterAxisHandler] id: {0} is not exist, abort...");
                return false;
            }

            m_AxisHandlerList.Remove(id);
            return true;
        }

        /// <summary>
        /// Registered button handlers with a button ID
        /// </summary>
        /// <param name="id">Button id registered in the profile</param>
        /// <param name="buttonHandler">Array of button handlers</param>
        /// <returns>Return true if successfully registered</returns>
        public bool RegisterButtonHandler(int id, params IButtonHandler[] buttonHandler)
        {
            if (m_ButtonHandlerList.ContainsKey(id))
            {
                Debug.LogWarningFormat("[InputSystemProfile::RegisterButtonHandler] id: {0} is already registered, abort...");
                return false;
            }

            m_ButtonHandlerList.Add(id, buttonHandler);
            return true;
        }

        /// <summary>
        /// Un-registered button handlers with a button ID
        /// </summary>
        /// <param name="id">Button id registered in the profile</param>
        /// <returns>Return true if successfully un-registered</returns>
        public bool UnRegisterButtonHandler(int id)
        {
            if (m_ButtonHandlerList.ContainsKey(id) == false)
            {
                Debug.LogWarningFormat("[InputSystemProfile::UnRegisterButtonHandler] id: {0} is not exist, abort...");
                return false;
            }

            m_ButtonHandlerList.Remove(id);
            return true;
        }

        #endregion
    }
}
