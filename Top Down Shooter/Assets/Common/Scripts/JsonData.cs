﻿// <copyright file="JsonData.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/24/2019 14:31:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 24, 2019

using UnityEngine;

namespace VuxtaStudio.Common
{
    /// <summary>
    /// Model class base that could be represent as json format
    /// </summary>
    [System.Serializable]
    public abstract class JsonData
    {
        /// <summary>
        /// Serialize to JSON format
        /// </summary>
        /// <returns></returns>
        public string SaveToJson()
        {
            return JsonUtility.ToJson(this);
        }
    }
}