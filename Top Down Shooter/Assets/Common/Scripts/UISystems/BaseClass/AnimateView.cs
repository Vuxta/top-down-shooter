﻿// <copyright file="AnimateView.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/24/2019 14:31:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 24, 2019

using UnityEngine;

namespace VuxtaStudio.Common.UISystems
{
    /// <summary>
    /// Use unity animator for UI view
    /// </summary>
    public class AnimateView : BaseView
    {
        [SerializeField]
        protected Animator m_Animator;

        public override void OnEnter(BaseViewContext context)
        {
            if (m_Animator == null)
                return;

            m_Animator.SetTrigger("OnEnter");
        }

        public override void OnExit(BaseViewContext context)
        {
            if (m_Animator == null)
                return;

            m_Animator.SetTrigger("OnExit");
        }

        public override void OnPause(BaseViewContext context)
        {
            if (m_Animator == null)
                return;

            //m_Animator.SetTrigger("OnPause");
            m_Animator.SetTrigger("OnExit");
        }

        public override void OnResume(BaseViewContext context)
        {
            if (m_Animator == null)
                return;

            //m_Animator.SetTrigger("OnResume");
            m_Animator.SetTrigger("OnEnter");
        }
    }
}
