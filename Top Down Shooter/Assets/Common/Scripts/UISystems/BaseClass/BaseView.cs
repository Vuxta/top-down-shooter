﻿// <copyright file="BaseView.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/24/2019 14:31:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 24, 2019

using UnityEngine;

namespace VuxtaStudio.Common.UISystems
{
    /// <summary>
    /// Base class for UI view
    /// </summary>
    public class BaseView : MonoBehaviour
    {
        public virtual void OnEnter(BaseViewContext context)
        {

        }

        public virtual void OnExit(BaseViewContext context)
        {

        }

        public virtual void OnPause(BaseViewContext context)
        {

        }

        public virtual void OnResume(BaseViewContext context)
        {

        }

        public void DestroySelf()
        {
            Destroy(gameObject);
        }
    }
}
