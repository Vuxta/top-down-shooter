﻿// <copyright file="ToggleItem.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/27/2019 10:50:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 27, 2019

using System;
using UnityEngine;
using UnityEngine.UI;

namespace VuxtaStudio.Common.UISystems
{
    /// <summary>
    /// Togglable UI items
    /// </summary>
    public class ToggleItem : MonoBehaviour
    {
        [SerializeField]
        private Toggle m_Toggle;

        [SerializeField]
        private Text m_LabelName;

        private Action<bool, string> m_OnValueChangeCallback;

        /// <summary>
        /// Init for toggle itmes
        /// </summary>
        /// <param name="name">Label name</param>
        /// <param name="callback">Callback when toggle value changed</param>
        public void Init(string name, Action<bool, string> callback)
        {
            m_LabelName.text = name;
            m_OnValueChangeCallback = callback;
            m_OnValueChangeCallback?.Invoke(m_Toggle.isOn, m_LabelName.text);
        }

        public void OnToggled()
        {
            m_OnValueChangeCallback?.Invoke(m_Toggle.isOn, m_LabelName.text);
        }
    }
}
