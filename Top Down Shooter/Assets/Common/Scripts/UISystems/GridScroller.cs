﻿// <copyright file="GridScroller.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/26/2019 15:01:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 26, 2019

using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;
using VuxtaStudio.Common.Extensions;

namespace VuxtaStudio.Common.UISystems
{
    /// <summary>
    /// Controller for achieve scrolling UI
    /// </summary>
    [RequireComponent(typeof(ScrollRect))]
    public class GridScroller : MonoBehaviour
    {

        // public UI elements //
        [SerializeField]
        private Transform m_ItemPrefab;
        [SerializeField]
        private GridLayoutGroup m_Grid;

        // private UI elements //
        private ScrollRect m_Scroller;

        // define //
        public enum Movement
        {
            Horizontal,
            Vertical,
        }

        // public fields //
        [SerializeField]
        private Movement m_MoveType = Movement.Horizontal;

        public delegate void OnChange(Transform trans, int index);

        // private fields //
        private HashSet<int> m_TransIndexSet = new HashSet<int>();
        private HashSet<int> m_ShowIndexSet = new HashSet<int>();
        private Dictionary<int, RectTransform> m_TransDict = new Dictionary<int, RectTransform>();
        private OnChange m_OnChange;
        private int m_ItemCount = 0;
        private int m_TransCount = 0;
        private int m_Col = 0;
        private int m_Row = 0;
        private Rect m_ScrollerRect;
        private bool m_HasChanged = false;
        private Vector2 m_CellSize = Vector3.zero;
        private Vector2 m_Spacing = Vector3.zero;

        public Vector2 ItemSize
        {
            get
            {
                return m_Spacing + m_CellSize;
            }
        }

        #region Public Methods

        public void Init(OnChange onChange, int itemCount, Vector2? normalizedPosition = null)
        {
            Clear();
            InitScroller();
            InitGrid();
            InitChildren(onChange, itemCount);
            InitTransform(normalizedPosition);
        }

        public void RefreshCurrent()
        {
            foreach (int index in m_TransIndexSet)
            {
                if (m_OnChange != null)
                {
                    m_OnChange(m_TransDict[index], index);
                }
            }
        }

        #endregion

        #region Init

        private void InitScroller()
        {
            // Init Scroller //
            m_Scroller = GetComponent<ScrollRect>();
            m_ScrollerRect = m_Scroller.GetComponent<RectTransform>().rect;

            if (m_MoveType == Movement.Horizontal)
            {
                m_Scroller.vertical = false;
                m_Scroller.horizontal = true;
            }
            else
            {
                m_Scroller.vertical = true;
                m_Scroller.horizontal = false;
            }
        }

        private void InitGrid()
        {
            m_CellSize = m_Grid.GetComponent<GridLayoutGroup>().cellSize;
            m_Spacing = m_Grid.GetComponent<GridLayoutGroup>().spacing;
            m_Grid.GetComponent<GridLayoutGroup>().enabled = false;

        }

        private void InitChildren(OnChange onChange, int itemCount)
        {
            m_OnChange = onChange;
            m_ItemCount = itemCount;
            m_Col = (int)((m_ScrollerRect.width + m_Spacing.x) / ItemSize.x);
            m_Row = (int)((m_ScrollerRect.height + m_Spacing.y) / ItemSize.y);
            if (m_MoveType == Movement.Horizontal)
            {
                m_Col += 2;
            }
            else
            {
                m_Row += 2;
            }

            if (m_Col <= 0)
                m_Col = 1;

            if (m_Row <= 0)
                m_Row = 1;

            m_TransCount = m_Col * m_Row;

            if (m_TransCount > m_ItemCount)
            {
                m_TransCount = m_ItemCount;
            }

            for (int i = 0; i < m_TransCount; i++)
            {
                Transform item = m_Grid.transform.AddChildFromPrefab(m_ItemPrefab, i.ToString());
                InitChild(item.GetComponent<RectTransform>(), i);
                m_OnChange(item, i);
                m_TransIndexSet.Add(i);
                m_TransDict.Add(i, item.GetComponent<RectTransform>());
            }
        }

        private void InitChild(RectTransform rectTrans, int index)
        {
            rectTrans.anchorMax = new Vector2(0, 1);
            rectTrans.anchorMin = new Vector2(0, 1);
            rectTrans.pivot = new Vector2(0, 1);
            rectTrans.sizeDelta = m_CellSize;
            rectTrans.anchoredPosition = IndexToPosition(index);
        }
        private void InitTransform(Vector2? normalizedPosition = null)
        {

            if (m_MoveType == Movement.Horizontal)
            {
                m_Grid.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, ((m_ItemCount + 1) / m_Row) * ItemSize.x);
            }
            else
            {
                m_Grid.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, ((m_ItemCount + 1) / m_Col) * ItemSize.y);
            }
            m_Scroller.onValueChanged.AddListener(OnValueChanged);
            m_Scroller.normalizedPosition = (normalizedPosition.HasValue) ? normalizedPosition.Value : new Vector2(0, 1);
        }

        public void Clear()
        {
            m_TransIndexSet.Clear();
            m_TransDict.Clear();
            m_ShowIndexSet.Clear();
            if (m_Grid != null)
            {
                m_Grid.GetComponent<RectTransform>().DestroyChildren();
            }
        }

        #endregion

        #region OnValueChanged

        public void OnValueChanged(Vector2 normalizedPosition)
        {
            if (m_TransCount == m_ItemCount)
            {
                return;
            }

            Vector2 scrollerSize = m_Scroller.GetComponent<RectTransform>().rect.size;
            Vector2 gridSize = m_Grid.GetComponent<RectTransform>().rect.size;

            int startIndex = 0;
            if (m_MoveType == Movement.Horizontal)
            {
                float scrollLength = -m_Grid.GetComponent<RectTransform>().anchoredPosition.x;
                int scrollCol = (int)(scrollLength / ItemSize.x);
                startIndex = scrollCol * m_Row;
            }
            else
            {
                float scrollLength = m_Grid.GetComponent<RectTransform>().anchoredPosition.y;
                int scrollRow = (int)(scrollLength / ItemSize.y);
                startIndex = scrollRow * m_Col;
            }

            SwapIndex(startIndex);
        }

        private void SwapIndex(int startIndex)
        {
            m_ShowIndexSet.Clear();
            for (int i = 0; i < m_TransCount; i++)
            {
                if ((i + startIndex) < m_ItemCount && (i + startIndex) >= 0)
                {
                    m_ShowIndexSet.Add(i + startIndex);
                }
            }

            if (m_ShowIndexSet.SetEquals(m_TransIndexSet))
            {
                return;
            }
            else
            {
                IEnumerator<int> lhsIter = m_ShowIndexSet.Except<int>(m_TransIndexSet).GetEnumerator();
                IEnumerator<int> rhsIter = m_TransIndexSet.Except<int>(m_ShowIndexSet).GetEnumerator();

                while (lhsIter.MoveNext() && rhsIter.MoveNext())
                {
                    ChangeToIndex(rhsIter.Current, lhsIter.Current);
                    m_OnChange(m_TransDict[lhsIter.Current], lhsIter.Current);
                }

                HashSet<int> tempSet = m_TransIndexSet;
                m_TransIndexSet = m_ShowIndexSet;
                m_ShowIndexSet = tempSet;
            }
        }

        private void ChangeToIndex(int from, int to)
        {
            //Debug.Log(from + " | " + to);
            RectTransform rectTrans = m_TransDict[from];
            rectTrans.anchoredPosition = IndexToPosition(to);
            m_TransDict.Remove(from);
            m_TransDict.Add(to, rectTrans);
        }

        private Vector2 IndexToPosition(int index)
        {
            if (m_MoveType == Movement.Horizontal)
            {
                return new Vector2(ItemSize.x * (index / m_Row), -ItemSize.y * (index % m_Row));
            }
            else
            {
                return new Vector2(ItemSize.x * (index % m_Col), -ItemSize.y * (index / m_Col));
            }
        }

        #endregion

    }
}
