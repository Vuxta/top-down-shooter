﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace VuxtaStudio.Common.Extensions
{
    public static class ListExtension
    {
        public static List<E> ShuffleList<E>(List<E> inputList)
        {
            List<E> randomList = new List<E>();

            System.Random r = new System.Random(Guid.NewGuid().GetHashCode());
            int randomIndex = 0;
            while (inputList.Count > 0)
            {
                randomIndex = r.Next(0, inputList.Count); //Choose a random object in the list
                randomList.Add(inputList[randomIndex]); //add it to the new, random list
                inputList.RemoveAt(randomIndex); //remove to avoid duplicates
            }

            return randomList; //return the new random list
        }
    }
}
