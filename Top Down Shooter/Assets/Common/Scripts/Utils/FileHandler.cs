﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VuxtaStudio.Common.Utils
{
    public static class FileHandler
    {
        public static bool WriteTo(string datas, string path)
        {
            try
            {
                if (!EvaluatePath(path))
                {
                    Debug.Log("Fail to create directory");
                    return false;
                }

                using (StreamWriter streamWriter = new StreamWriter(path, false))
                {
                    streamWriter.WriteLine(datas);
                    streamWriter.Close();
                }

                Debug.LogFormat("File saved to : {0}", path);

                return true;
            }
            catch (Exception e)
            {
                Debug.LogErrorFormat("Fail to write environment datas: {0}", e.Message);
                return false;
            }
        }

        public static string ReadFrom(string path)
        {
            string result = string.Empty;

            if (File.Exists(path))
            {
                using (StreamReader streamReader = new StreamReader(path))
                {
                    result = streamReader.ReadLine();
                    Debug.LogFormat("Read data: {0}", result);
                }
            }
            else
            {
                Debug.LogWarningFormat("File does not exist: {0}", path);
            }

            return result;
        }

        public static bool EvaluatePath(string path)
        {

            try
            {
                string folder = Path.GetDirectoryName(path);
                if (!Directory.Exists(folder))
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(folder);
                }
            }
            catch (IOException ioex)
            {
                Console.WriteLine(ioex.Message);
                return false;
            }

            return true;
        }
    }
}
