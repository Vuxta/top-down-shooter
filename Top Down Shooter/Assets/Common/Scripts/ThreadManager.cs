﻿// <copyright file="ThreadManager.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>12/3/2018 15:12:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, December 3, 2018

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

namespace VuxtaStudio.Common
{
    /// <summary>
    /// Multithread helper manager
    /// </summary>
    public class ThreadManager : MonoSingleton<ThreadManager>
    {
        #region Structs

        public struct DelayedQueueItem
        {
            public Action action;
            public float time;
        }

        #endregion

        #region Private Fields

        private readonly int m_MaxThreads = 8;

        private static int m_NumThreads;

        private List<Action> m_Actions = new List<Action>();
        private List<Action> m_CurrentActions = new List<Action>();
        private List<DelayedQueueItem> m_CurrentDelayed = new List<DelayedQueueItem>();
        private List<DelayedQueueItem> m_Delayed = new List<DelayedQueueItem>();

        #endregion

        #region Public Methods

        /// <summary>
        /// Queue input action to be execute on main unity thread
        /// </summary>
        /// <param name="action">Callback action</param>
        public void QueueOnMainThread(Action action)
        {
            QueueOnMainThread(action, 0f);
        }

        /// <summary>
        /// Queue input action to be execute on main unity thread with a delay of time
        /// </summary>
        /// <param name="action">Callback actions</param>
        /// <param name="time">delayed time</param>
        public void QueueOnMainThread(Action action, float time)
        {
            if (!Mathf.Approximately(time, 0))
            {
                lock (Instance.m_Delayed)
                {
                    Instance.m_Delayed.Add(new DelayedQueueItem { time = Time.time + time, action = action });
                }
            }
            else
            {
                lock (Instance.m_Actions)
                {
                    Instance.m_Actions.Add(action);
                }
            }
        }

        /// <summary>
        /// Queue action callback on Thread pool and run async
        /// </summary>
        /// <param name="action">Callback action</param>
        /// <returns></returns>
        public Thread RunAsync(Action action)
        {
            while (m_NumThreads >= m_MaxThreads)
            {
                Thread.Sleep(1);
            }

            Interlocked.Increment(ref m_NumThreads);
            ThreadPool.QueueUserWorkItem(RunAction, action);

            return null;
        }

        #endregion

        #region Private Methods

        private void RunAction(object action)
        {
            try
            {
                ((Action)action)();
            }
            finally
            {
                Interlocked.Decrement(ref m_NumThreads);
            }
        }

        private void Update()
        {
            lock (m_Actions)
            {
                m_CurrentActions.Clear();
                m_CurrentActions.AddRange(m_Actions);
                m_Actions.Clear();
            }

            foreach (var action in m_CurrentActions)
            {
                action();
            }

            lock (m_Delayed)
            {
                m_CurrentDelayed.Clear();
                m_CurrentDelayed.AddRange(m_Delayed.Where(d => d.time <= Time.time));

                foreach (var item in m_CurrentDelayed)
                    m_Delayed.Remove(item);
            }

            foreach (var delayed in m_CurrentDelayed)
            {
                delayed.action();
            }
        }

        #endregion
    }
}
